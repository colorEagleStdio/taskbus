cmake_minimum_required(VERSION 3.10)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 COMPONENTS Core REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core REQUIRED)
include_directories(${TASKBUS_INTERFACEDIR})
set(PRJ_SOURCES
	iio.cpp
	listen_thread.cpp
	main.cpp
	resources.qrc
)

if(CMAKE_SYSTEM_NAME MATCHES "Windows")
	include_directories(${TASKBUS_THIRDIR}/win32/libiio/include)
	if(MSVC)
	set(IIO_LIB iio)
	if(TB64BIT)
		link_directories(${TASKBUS_THIRDIR}/win32/libiio/MS64)
	else()
		link_directories(${TASKBUS_THIRDIR}/win32/libiio/MS32)
	endif()
	else()
	set(IIO_LIB iio.dll)
	if(TB64BIT)
		link_directories(${TASKBUS_THIRDIR}/win32/libiio/MinGW64)
	else()
		link_directories(${TASKBUS_THIRDIR}/win32/libiio/MinGW32)
	endif()
	endif()
else()
	set(IIO_LIB iio)
endif()
#############Target======================

add_executable(sink_plutosdr
	${PRJ_SOURCES}
)
target_link_libraries(sink_plutosdr
	${IIO_LIB}
	Qt${QT_VERSION_MAJOR}::Core
	${TBLinkExtra}
)

# ====copy doc
add_custom_command(TARGET sink_plutosdr
		 POST_BUILD
		 COMMAND echo Copy Handbook to EXE path
		 COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/sink_plutosdr.md ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/sink_plutosdr.md
	 )
