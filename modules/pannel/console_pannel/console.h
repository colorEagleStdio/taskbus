// Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
// Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef CONSOLE_H
#define CONSOLE_H

#include <QPlainTextEdit>
#include <QByteArray>
class Console : public QPlainTextEdit
{
    Q_OBJECT

signals:
    void getData(const QByteArray &data);

public:
    explicit Console(QWidget *parent = nullptr);

	void putData(const QByteArray &data,int locale = 0);
    void setLocalEchoEnabled(bool set);

protected:
	void insertFromMimeData(const QMimeData *source) override;
    void keyPressEvent(QKeyEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseDoubleClickEvent(QMouseEvent *e) override;
    void contextMenuEvent(QContextMenuEvent *e) override;
	void inputMethodEvent(QInputMethodEvent *event) override;
private:
	bool m_localEchoEnabled = true;	
};

#endif // CONSOLE_H
