﻿#ifndef DIALOGPANNEL_H
#define DIALOGPANNEL_H

#include <QDialog>
#include <QByteArray>
#include <QMap>
#include <QVector>
#include <QStandardItemModel>
#include <QByteArray>
#include "listen_thread.h"
#include "cmdlineparser.h"

class QGridLayout;

namespace Ui {
	class DialogPannel;
}

class DialogPannel : public QDialog
{
	Q_OBJECT

public:
	explicit DialogPannel(const TASKBUS::cmdlineParser * ps, QWidget *parent = 0);
	~DialogPannel();
	void timerEvent(QTimerEvent *event) override;
private:
	Ui::DialogPannel *ui;
	QStandardItemModel * m_log;
	const TASKBUS::cmdlineParser * m_pCmd = nullptr;
	reciv_thread * m_rthread = nullptr;
	QByteArray m_arr_buf;
	int m_nTimer = 0;
private:
	int sub_output = 0;
	int instance = 0;
	int m_maxPackSize = 160;
	int m_locale = 0;
	QByteArray m_displayQueue;
private slots:
	void deal_message(QString);
	void deal_console_input(QByteArray);
	void deal_taskbus_input(QByteArray);
};

#endif // DIALOGPANNEL_H
