#include "dialogpannel.h"
#include <QApplication>
#include <QTextStream>
#include "cmdlineparser.h"
#include "tb_interface.h"
#include <QFile>
using namespace TASKBUS;
//#define OFFLINEDEBUG

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	init_client();

#ifdef OFFLINEDEBUG
	FILE * old_stdin, *old_stdout;
	auto ars = debug("Z:\\pid13300",&old_stdin,&old_stdout);
	const  cmdlineParser args (ars);
#else
	const cmdlineParser args (argc,argv);
#endif


	int ret = 0;

	//每个模块要响应 --information参数,打印自己的功能定义字符串。或者提供一个json文件。
	if (args.contains("information"))
	{
		QFile fp(":/json/console_pannel.exe.json");
		fp.open(QIODevice::ReadOnly);
		if (fp.isOpen())
		{
			QByteArray arr = fp.readAll();
			arr.push_back('\0');
			puts(arr.constData());
			fflush(stdout);
		}
		ret = -1;
	}
	else
	{
		DialogPannel w(&args);
		w.show();
		if (args.toInt("hide_window",0)!=0)
			w.hide();
		ret = a.exec();
	}
	return ret;
}
