﻿#include "listen_thread.h"
#include "tb_interface.h"
#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QDateTime>
#ifdef WIN32
#include <io.h>
#include <fcntl.h>
#endif
reciv_thread::reciv_thread(const TASKBUS::cmdlineParser * pCmd,QObject *parent)
	:QThread(parent)
	,m_pCmd(pCmd)
{

}

void reciv_thread::run()
{
	using namespace TASKBUS;
	bool bfinished = false;
	QDateTime dtmLastCmt = QDateTime::currentDateTime();
	while (false==bfinished)
	{
		subject_package_header header;
		std::vector<unsigned char> packagedta = pull_subject(&header);
		if (false==is_valid_header(header))
		{
			continue;
		}
		if ( is_control_subject(header) && packagedta.size())
		{
			//收到命令进程退出的广播消息,退出
			if (strstr(control_subject(header,packagedta).c_str(),"function=quit;"))
			{
				bfinished = true;
				qDebug()<<"Quit!";
				emit sig_quit();
			}
			else
			{
				QByteArray arr((char *)packagedta.data(),packagedta.size());
				emit new_message(QString::fromUtf8(arr));
			}
		}
		else
		{
			QByteArray arr((char *)packagedta.data(),packagedta.size());
			emit new_data(arr);
		}
	}

	return ;
}

