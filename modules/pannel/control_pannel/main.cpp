#include "dialogpannel.h"
#include <QApplication>
#include <QTextStream>
#include "cmdlineparser.h"
#include "tb_interface.h"
#include <QFile>
/*! TASKBUS控制面板
 * TASKBUS的模块可以通过广播，发布自己可以配置的参数。格式：
 * 广播专题=0xffffffffu,也就是 TB_SUBJECT_CMD
 * 通路：0
 * 内容：
 *	function=handle_annouce;
 *  source=进程的instance_id
 *  //所有模块，或者可以填写pannel模块的instance_id
 *  destin=0;
 *	handle=变量名;
 *  label=控件的名字，如果不提供，就是handel.
 *	type=[int/double/string];
 *  step=单步;
 *  decimal=小数位数;
 *  //范围，不提供，则创建的是文本框
 *	range=min:max;/range=a,b,c,d,...;
 *  value=当前值;
 *  ==============================================
 *  本模块收到这样的声明，会为这个请求创建一个控件。
 *  当控件的值被修改时，向对应模块发送消息
 *	function=handle_set;
 *  source=pannel进程的instance_id;
 *  destin=handle进程的instance_id;
 *	handle=变量名;
 *  value=当前值;
 *  ==============================================
 *  当handle模块的变量发生改变后，可以更新本模块的显示
 *  function=handle_set;
 *  source=handle进程的instance_id;
 *  destin=pannel进程的instance_id;
 *	handle=变量名;
 *  value=当前值;
 *
 */

using namespace TASKBUS;
//#define OFFLINEDEBUG

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	init_client();

#ifdef OFFLINEDEBUG
	FILE * old_stdin, *old_stdout;
	auto ars = debug("/home/user/codes/build-taskbus-qt6-Debug/bin/debug/pid25106/",&old_stdin,&old_stdout);
	const  cmdlineParser args (ars);
#else
	const cmdlineParser args (argc,argv);
#endif


	int ret = 0;

	//每个模块要响应 --information参数,打印自己的功能定义字符串。或者提供一个json文件。
	if (args.contains("information"))
	{
		QFile fp(":/json/control_pannel.exe.json");
		fp.open(QIODevice::ReadOnly);
		if (fp.isOpen())
		{
			QByteArray arr = fp.readAll();
			arr.push_back('\0');
			puts(arr.constData());
			fflush(stdout);
		}
		ret = -1;
	}
	else
	{
		DialogPannel w(&args);
		w.show();
		if (args.toInt("hide_window",0)!=0)
			w.hide();
		ret = a.exec();
	}
	return ret;
}
