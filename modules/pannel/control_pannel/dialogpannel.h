﻿#ifndef DIALOGPANNEL_H
#define DIALOGPANNEL_H

#include <QDialog>
#include <QByteArray>
#include <QMap>
#include <QVector>
#include <QStandardItemModel>
#include <functional>
#include "listen_thread.h"
#include "cmdlineparser.h"

class QGridLayout;

namespace Ui {
	class DialogPannel;
}

class DialogPannel : public QDialog
{
	Q_OBJECT

public:
	explicit DialogPannel(const TASKBUS::cmdlineParser * ps, QWidget *parent = 0);
	~DialogPannel();
private:
	Ui::DialogPannel *ui;
	QStandardItemModel * m_log;
	const TASKBUS::cmdlineParser * m_pCmd = nullptr;
	reciv_thread * m_rthread = nullptr;
protected:
	//Source, tabs
	QMap<QString, QGridLayout * > m_tabs;
	//Source ,handel,Widgets
	QMap<QString, QMap<QString, QWidget * > > m_ctrls;
	//Source ,handel,Handles
	QMap<QString, QMap<QString, std::function<void (QString)> > > m_handles;
private slots:
	void deal_message(QString);
private:
	void AddHandle(std::map<std::string, std::string> & cmd);
	void HandleChanged(QString source,QString handle,QString value);
};

#endif // DIALOGPANNEL_H
