#-------------------------------------------------
#
# Project created by QtCreator 2018-07-27T16:02:44
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += ../../../tb_interface
DESTDIR = $$OUT_PWD/../../../bin/modules
TARGET = control_pannel
TEMPLATE = app
include (../../../taskbusplatform.pri)
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
INCLUDEPATH += "../../../tb_interface"

SOURCES += \
	main.cpp \
	dialogpannel.cpp \
	listen_thread.cpp

HEADERS += \
	dialogpannel.h \
	listen_thread.h

FORMS += \
	dialogpannel.ui

DISTFILES += \
	control_pannel.exe.json \
	control_pannel.zh_CN.json

RESOURCES += \
	control_pannel.qrc

QMAKE_POST_LINK += $${QMAKE_COPY} $$PWD/control_pannel.md $$DESTDIR/control_pannel.md
