# 控制面板模块

首次发布：2020-06

最新发布：2022-11

作者：彩鹰工作室

----

控制面板模块用于统一管理各个进程开放的可配置参数表。典型的需求是在模块运行时，动态的修改/显示某个参数。如在调频收音机启动时，动态改变频率、增益。

这个模块的原理参考技术文章：[Qt lambda槽在跨进程功能发现与参数同步中的应用](https://goldenhawking.blog.csdn.net/article/details/124426795)


# 1.输入输出专题

本模块没有输入输出专题。

# 2.工作原理

要做到跨进程参数的实时控制，需要经过如下几个步骤。

1. 参数发布：受控模块要广播自身可被配置的参数。
2. 参数更新，包含两个部分。
	1. 参数显示：控制面板模块收到广播，创建控件并显示。
	2. 参数配置：控制面板模块中的控件被用户点击，取值发生改变。此时，会广播模块修改命令。


## 2.1 参数发布

参数发布使用taskBus的控制专题(0xffffffffu)进行全局广播。控制专题是一种特殊的专题，能够实现模块之间的控制消息传递。传递的消息格式是key=value这样的样式，用分号分割。

一个模块要告诉所有部位，自己在运行时，有哪些可操作的变量。同时，要广播的还有变量的类型、范围，取值等等。这个指令可以看做功能声明，指令类似：

```txt
source=2;
destin=0;
function=handle_annouce;
handle=rx_freq;
label=接收频率(Hz);
type=double;
range=0:6000000000;
step=100000;
decimal=0;
value=70000000;
```

或者：

```txt
source=2;
destin=0;
function=handle_annouce;
handle=rx_antenna;
label=接收天线;
type=string;
range=RX,TX/RX;
value=RX2;
```


当模块启动时，向所有人广播这个功能声明，以便发现相应的功能。

上述字符串使用专题0xffffffffu直接写入 stdout，即可完成声明。当然，如果您使用的是C语言，在 tb_interface.h里，已经有了方便的函数直接使用：

```c
	/*!
	 * \brief annouce_handle 通过广播，公告某个参数可被全局配置
	 * \param source	本公告的来源。可以自定义，只要确保与配置指令的一致性即可。
	 * \param destin	一般填“0”，表示向所有模块实例广播
	 * \param handle	参数的名称（句柄名称），如频率、增益
	 * \param value		当前取值
	 * \param label		显示在界面上的友好信息。如果不写，handle会被显示
	 * \param type		类型，可以为空，则表示字符串。
	 * \param range		取值范围：用于指定spinbox的范围
	 * \param step		步进：用于指定spinbox的步进
	 * \param decimal	小数位数。
	 * \param pMtx		用于多线程stdout时的互斥量
	 */
	inline void annouce_handle(
		const std::string & source,
		const std::string & destin,
		const std::string & handle,
		const std::string & value,
		const std::string & label = "",
		const std::string & type = "",
		const std::string & range = "",
		const std::string & step = "",
		const std::string & decimal = "",
		std::mutex * pMtx = nullptr
			)...
```

## 2.2 参数更新

一旦要进行参数同步，则由修改方发起，发给被修改方。这种修改可以是双向的，即参数使用进程如果发现参数变更，可以发起同步给界面控制台。界面控制台接到修改请求，发给参数使用进程。指令：


```txt
source=7;
destin=2;
function=handle_set;
handle=rx_freq;
value=71200000;
```

当然，使用者要保证destin的正确性。

