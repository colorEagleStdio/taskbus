TEMPLATE = subdirs

#main framework project
SUBDIRS  += \
    pannel/control_pannel \
    pannel/console_pannel \
    sources/source_files \
    sources/source_plutosdr \
    transforms/transform_fft \
    transforms/mod_fm \
    transforms/mod_fm_dem \
    transforms/filter_fir \
    transforms/resample_pqfraction \
    sinks/sink_file \
    sinks/sink_plutosdr \
    sinks/sink_SQL \
    network/network_p2p \
	network/network_tcp \
	network/network_udp \
    wrappers/wrapper_scripts \
    wrappers/wrapper_stdio \
    uhd/uhd_usrp_continous \
    uhd/uhd_usrp_io

#检查QtChart模块
qtHaveModule(charts){
message("charts found, sink_plots enabled.")
SUBDIRS  += \
    sinks/sink_plots
} else {
message("charts NOT Found, sink_plots disabled.")
}
#检查多媒体模块
qtHaveModule(multimedia){
message("multimedia Found, soundcard enabled.")
SUBDIRS  += \
    sinks/sink_soundcard \
    sources/source_soundcard
} else {
message("multimedia NOT Found, soundcard disabled.")
}
#检查串口模块
qtHaveModule(serialport){
message("serialport Found, network_serialport enabled.")
SUBDIRS  += \
	network/network_serialport
} else {
message("serialport NOT Found, network_serialport disabled.")
}

