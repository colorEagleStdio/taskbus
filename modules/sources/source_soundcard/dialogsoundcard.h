#ifndef DIALOGSOUNDCARD_H
#define DIALOGSOUNDCARD_H

#include <QDialog>
#include <QAudioInput>
#include <QAudioOutput>
#include <QAudioDevice>
#include <QList>
#include <QStandardItemModel>
#include <QAudioSource>
#include "cmdlineparser.h"
#include "tb_interface.h"
#include "listen_thread.h"
namespace Ui {
	class DialogSoundCard;
}

class DialogSoundCard : public QDialog
{
	Q_OBJECT

public:
	explicit DialogSoundCard(QWidget *parent = nullptr);
	explicit DialogSoundCard(const TASKBUS::cmdlineParser * pline,QWidget *parent = nullptr);
	~DialogSoundCard();
	void setInstance(const int i){m_n_instance = i;}
private:
	Ui::DialogSoundCard *ui;
	QList<QAudioDevice> m_devInputlist;
	QStandardItemModel * m_devInputModule = nullptr;

private:
	int AddWavHeader(char *);
	inline int ApplyVolumeToSample(short * iSample, int len)
	{
		miMaxValue = 0;
		//Calculate volume, Volume limited to  max 30000 and min -30000
		for (int i=0;i<len;++i)
		{
			iSample[i] =  std::max(std::min(((iSample[i] * miVolume) / 50) ,30000), -30000);
			if (miMaxValue < abs(iSample[i]))
				miMaxValue = abs(iSample[i]);
		}
		return miMaxValue;
	}
	void InitMonitor();
	void CreateAudioInput();
private slots:
	void OnRecordStart();
	void OnRecordStop();
	void OnStateChange(QAudio::State s);
	void OnReadMore();
	void OnSliderValueChanged(int);
	void OnTimeOut();

private:
	int m_n_instance = 0;
	int m_batch_size = 0;
	int miVolume;
	int miMaxValue;
	QByteArray m_buffer;
	quint64 m_nTotalSps = 0;
	const TASKBUS::cmdlineParser * m_cmdline = nullptr;
	listen_thread * m_pListenThread = nullptr;
private:
	QAudioFormat mFormatSound;
	QAudioSource *mpAudioInputSound;		// 负责监听声音
	QIODevice *mpInputDevSound;

};

#endif // DIALOGSOUNDCARD_H
