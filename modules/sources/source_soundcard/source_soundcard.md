# 声卡连续录制模块

文档日期：2024-05


作者：彩鹰工作室

----

该模块使用声卡作为输入设备，录制声音。

## 1 支持的硬件类型

支持操作系统内可被 Qt Multimedia 枚举的类型。

## 2 设置的参数

|参数名|意义|建议取值|
|---|---|---|
|sample_rate|采样率(Hz)|默认是44100Hz|
|device|设备名称|默认为"default"|
|channel|声道数|1为单声道，2为立体声|
|hide|运行时是否隐藏|0是显示，1是隐藏|
|autostart|运行即开始|0需要手工开始，1为立刻开始|

## 3 输入：

timestamp_in 用于和声卡输出的timestamp配合，防止内存过度膨胀。

## 输出：

wav 是输入的16位有符号整形。立体声左声道+右声道交叉排列。
timestamp 输出从首个样点开始的样点总数。

