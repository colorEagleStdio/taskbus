#ifndef LISTEN_THREAD_H
#define LISTEN_THREAD_H

#include <QThread>
#include <atomic>
class listen_thread: public QThread
{
	Q_OBJECT
public:
	explicit listen_thread(QObject * parent);
protected:
	void run() override;
signals:
	void quit_app();
};

extern int i_watermark /*=0*/;
extern std::atomic<long long> g_watermark /*=0*/;
#endif // LISTEN_THREAD_H
