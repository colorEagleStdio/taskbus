TEMPLATE = app
QT -= gui
CONFIG += c++11 console
CONFIG -= app_bundle
include (../../../taskbusplatform.pri)
INCLUDEPATH += ../../../tb_interface
DESTDIR = $$OUT_PWD/../../../bin/modules
SOURCES += \
	main.cpp
RESOURCES += info.qrc

