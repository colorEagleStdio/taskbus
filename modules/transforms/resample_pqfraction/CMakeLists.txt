cmake_minimum_required(VERSION 3.10)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 COMPONENTS Core REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core REQUIRED)
include_directories(${TASKBUS_INTERFACEDIR})
set(PRJ_SOURCES
	ds_bessel.h
	resample.h
	upfirdn.h
	main.cpp
	resample_pqfraction.qrc
)
#############Target======================

add_executable(resample_pqfraction
	${PRJ_SOURCES}
)
target_link_libraries(resample_pqfraction
	Qt${QT_VERSION_MAJOR}::Core
	${TBLinkExtra}
)

