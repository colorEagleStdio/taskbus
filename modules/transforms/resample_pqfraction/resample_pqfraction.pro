TEMPLATE = app
QT -= gui
CONFIG += c++17 console
CONFIG -= app_bundle
include (../../../taskbusplatform.pri)
INCLUDEPATH += ../../../tb_interface
DESTDIR = $$OUT_PWD/../../../bin/modules
SOURCES += \
	main.cpp
HEADERS += \
	ds_bessel.h \
	upfirdn.h \
	resample.h
RESOURCES += resample_pqfraction.qrc

