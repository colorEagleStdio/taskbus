cmake_minimum_required(VERSION 3.10)

add_subdirectory(mod_fm)
add_subdirectory(mod_fm_dem)
add_subdirectory(transform_fft)
add_subdirectory(filter_fir)
add_subdirectory(resample_pqfraction)
