cmake_minimum_required(VERSION 3.10)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 COMPONENTS Core Widgets SerialPort REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core  Widgets  SerialPort REQUIRED)

include_directories(${TASKBUS_INTERFACEDIR})

qt_add_executable(network_serialport
	MANUAL_FINALIZATION
	main.cpp
	listen_thread.h listen_thread.cpp
	mainwindow.cpp mainwindow.h mainwindow.ui
	settingsdialog.cpp settingsdialog.h settingsdialog.ui
	network_serialport.qrc
)

set_target_properties(network_serialport PROPERTIES
	WIN32_EXECUTABLE TRUE
	MACOSX_BUNDLE TRUE
)

target_link_libraries(network_serialport PRIVATE
	Qt${QT_VERSION_MAJOR}::Widgets
	Qt${QT_VERSION_MAJOR}::SerialPort
	${TBLinkExtra}
)
qt_finalize_executable(network_serialport)
