// Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
// Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include "cmdlineparser.h"
#include "tb_interface.h"
#include "listen_thread.h"
QT_BEGIN_NAMESPACE

class QLabel;
class QTimer;

namespace Ui {
class MainWindow;
}

QT_END_NAMESPACE

class Console;
class SettingsDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
	explicit MainWindow(const TASKBUS::cmdlineParser * cmdLine, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void openSerialPort();
    void closeSerialPort();
    void about();
    void writeData(const QByteArray &data);
    void readData();

    void handleError(QSerialPort::SerialPortError error);
    void handleBytesWritten(qint64 bytes);
    void handleWriteTimeout();
	void on_pushButton_send_clicked();
	void on_pushButton_send_all_clicked();


private:
    void initActionsConnections();
	QString ba2str(QByteArray arr);
	QByteArray str2ba(QString arr);
private:
    void showStatusMessage(const QString &message);
    void showWriteError(const QString &message);

    Ui::MainWindow *m_ui = nullptr;
    QLabel *m_status = nullptr;
    SettingsDialog *m_settings = nullptr;
    qint64 m_bytesToWrite = 0;
    QTimer *m_timer = nullptr;
    QSerialPort *m_serial = nullptr;
private:
	const TASKBUS::cmdlineParser * cmdline = nullptr;
	reciv_thread * m_pRevThd = nullptr;
private slots:
	void slot_new_taskpack(QByteArray);

};

#endif // MAINWINDOW_H
