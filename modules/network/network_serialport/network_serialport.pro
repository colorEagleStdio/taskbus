QT += widgets serialport
requires(qtConfig(combobox))
INCLUDEPATH += ../../../tb_interface
DESTDIR = $$OUT_PWD/../../../bin/modules

TARGET = network_serialport
TEMPLATE = app
include (../../../taskbusplatform.pri)
SOURCES += \
    main.cpp \
    mainwindow.cpp \
    settingsdialog.cpp \
	listen_thread.cpp

HEADERS += \
    mainwindow.h \
    settingsdialog.h \
	listen_thread.h

FORMS += \
    mainwindow.ui \
    settingsdialog.ui

RESOURCES += \
    network_serialport.qrc

