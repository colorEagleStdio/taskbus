关于UDP传输可靠性调优

在Windows下，默认的UDP套接字的缓存比较小，使用时容易丢包。实际进行UDP实时吞吐时，建议调整默认的UDP套接字缓存为较大的值。

可以导入 FastUDPRegSetup.reg 中的配置，并重启计算机。