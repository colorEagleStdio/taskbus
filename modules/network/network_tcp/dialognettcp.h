#ifndef DIALOGNETTCP_H
#define DIALOGNETTCP_H

#include <QDialog>
#include <QSet>
#include <QStandardItemModel>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMap>
#include <QVector>
#include "listen_thread.h"
namespace Ui {
	class DialogNetTCP;
}

class DialogNetTCP : public QDialog
{
	Q_OBJECT

public:
	explicit DialogNetTCP(const int ins,QWidget *parent = 0);
	~DialogNetTCP();
	void setNetPara(const QString addr, const int port, const int mod, int in, int out);
	void startWork();
protected:
	void timerEvent(QTimerEvent *event);

private:
	Ui::DialogNetTCP *ui = nullptr;

	QTcpServer * m_svr = nullptr;
	QTcpSocket * m_sock = nullptr;

	QString m_str_addr = "127.0.0.1";
	int	m_n_port = 12800;
	int m_n_mod = 0;
	int m_n_instance = 0;
	int m_in = 0;
	int m_out = 0;

	int m_nTimer = -1;

	reciv_thread * m_pRevThd = nullptr;

	QTcpSocket::SocketState m_lastState = QTcpSocket::UnconnectedState;


protected slots:
	void slot_new_connection();
	void slot_read_sock();
	void slot_new_taskpack(QByteArray);
private slots:
	void on_pushButton_listerner_start_clicked();
	void on_checkBox_listener_positive_stateChanged(int arg1);
	void on_lineEdit_listenerAddr_textChanged(const QString &arg1);
	void on_lineEdit_listenerPort_textChanged(const QString &arg1);
};

#endif // DIALOGNETTCP_H
