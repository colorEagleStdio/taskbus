﻿#include "dialognettcp.h"
#include "ui_dialognettcp.h"
#include <QDateTime>
#include <QMessageBox>
#include <QSettings>
#include <QTextStream>
#include "tb_interface.h"
DialogNetTCP::DialogNetTCP(const int ins,QWidget *parent)
	:QDialog(parent)
	,ui(new Ui::DialogNetTCP)
	,m_svr( new QTcpServer(this))
	,m_n_instance(ins)
	,m_pRevThd(new reciv_thread(this))
{
	ui->setupUi(this);
	connect(m_svr,&QTcpServer::newConnection,this,&DialogNetTCP::slot_new_connection);
	connect(m_pRevThd,&reciv_thread::new_package,this,&DialogNetTCP::slot_new_taskpack,Qt::QueuedConnection);
	connect(m_pRevThd,&reciv_thread::sig_quit,this,&DialogNetTCP::close);
	m_pRevThd->start();
}

DialogNetTCP::~DialogNetTCP()
{
	m_pRevThd->terminate();
	delete ui;
}



void  DialogNetTCP::timerEvent(QTimerEvent * e)
{
	if (e->timerId()==m_nTimer)
	{
		if (m_n_mod)
		{
			if (m_sock)
			{
				if (m_lastState!=m_sock->state())
				{
					fprintf(stderr,"Socket State: %d->%d\n", m_lastState,m_sock->state());
					fflush(stderr);
					m_lastState = m_sock->state();
				}
				if(m_sock->state()!=QTcpSocket::ConnectedState &&
						m_sock->state()!=QTcpSocket::ConnectingState &&
						m_sock->state()!=QTcpSocket::HostLookupState
						)
				{
					m_sock->abort();
					m_sock->connectToHost(QHostAddress(m_str_addr),m_n_port);
				}

			}
		}
		else
		{
			if (!m_svr->isListening())
			{
				if (m_svr->listen(QHostAddress(m_str_addr),m_n_port))
				{
					fprintf(stderr,"Listening on port %d\n",m_n_port);
					fflush(stderr);
					TASKBUS::push_subject(TB_SUBJECT_CMD,0,
										  QString("source=%1;"
										  "destin=0;"
										  "function=listening;"
										  "hostaddr=%2;"
										  "port=%3;"
										  "class=network_tcp.taskbus;"
										  )
										  .arg(m_n_instance)
										  .arg(m_str_addr)
										  .arg(m_n_port).toStdString().c_str());
				}
				else
				{
					fprintf(stderr,"Can not open port %d\n",m_n_port);
					fflush(stderr);
				}

			}
		}
		static int cc = 0;
		if ((++cc)%10==0)
			TASKBUS::push_subject(TB_SUBJECT_CMD,0,
								  QString("source=%1;"
										  "destin=0;"
										  "function=aloha;"
										  "class=network_tcp.taskbus;"
										  )
								  .arg(m_n_instance).toStdString().c_str());
	}
	return QDialog::timerEvent(e);
}

void DialogNetTCP::slot_new_connection()
{
	static QTextStream stmerr(stderr);
	while (m_svr->hasPendingConnections())
	{
		QTcpSocket * sock = m_svr->nextPendingConnection();
		if (m_sock)
		{
			m_sock->abort();
			m_sock->deleteLater();
			stmerr<<"Warning: This module only allow 1 tcp connection at the same time. Old connection is dropped.\n";
		}
		m_sock = sock;
		connect(sock,&QTcpSocket::readyRead,this,&DialogNetTCP::slot_read_sock);
		stmerr << "Incoming connection from "<<sock->peerName()<< "("<<sock->peerAddress().toString()<<":"<<sock->peerPort()<<");\n";
		stmerr.flush();
	}
}

void DialogNetTCP::setNetPara(const QString addr, const int port, const int mod, int in, int out)
{
	m_str_addr = addr;
	m_n_port = port;
	m_n_mod = mod;
	m_in = in;
	m_out= out;
	ui->lineEdit_listenerAddr->setText(m_str_addr);
	ui->lineEdit_listenerPort->setText(QString("%1").arg(port));
	ui->checkBox_listener_positive->setChecked(mod==0?true:false);
}

void DialogNetTCP::on_pushButton_listerner_start_clicked()
{
	startWork();
}

void DialogNetTCP::startWork()
{
	ui->pushButton_listerner_start->setDisabled(true);
	if (m_nTimer<0)
	{
		if (m_n_mod==0)
		{
			if (m_svr->isListening())
				m_svr->close();
			if (true==m_svr->listen(QHostAddress(m_str_addr),m_n_port))
			{
				fprintf(stderr,"Listening on port %d\n",m_n_port);
				fflush(stderr);
			}
			else
			{
				fprintf(stderr,"Can not open port %d\n",m_n_port);
				fflush(stderr);
			}
		}
		else
		{
			if (m_sock)
			{
				m_sock->abort();
				m_sock->deleteLater();
			}
			m_sock = new QTcpSocket(this);
			m_sock->connectToHost(QHostAddress(m_str_addr),m_n_port);
			connect(m_sock,&QTcpSocket::readyRead,this,&DialogNetTCP::slot_read_sock);
			fprintf(stderr,"connecting to: %s:%d\n", m_str_addr.toStdString().c_str(), m_n_port);
			fflush(stderr);
		}
		m_nTimer=startTimer(1000);
	}
}

void DialogNetTCP::on_checkBox_listener_positive_stateChanged(int /*arg1*/)
{
	m_n_mod = ui->checkBox_listener_positive->isChecked()?0:1;
}

void DialogNetTCP::on_lineEdit_listenerAddr_textChanged(const QString &/*arg1*/)
{
	m_str_addr = ui->lineEdit_listenerAddr->text();
}

void DialogNetTCP::on_lineEdit_listenerPort_textChanged(const QString &/*arg1*/)
{
	m_n_port = ui->lineEdit_listenerPort->text().toInt();
}



void DialogNetTCP::slot_read_sock()
{
	using namespace TASKBUS;
	QByteArray arrData = m_sock->readAll();
	if (m_in)
		push_subject(
					m_out,m_n_instance,
					arrData.length(),(const unsigned char *)arrData.constData()
					);
}


void DialogNetTCP::slot_new_taskpack(QByteArray package)
{
	using namespace TASKBUS;
	const subject_package_header * pheader = (const subject_package_header *)
			package.constData();
	const int pts = pheader->data_length;
	const unsigned char * fdata =  (const unsigned char *)(package.constData()+sizeof(subject_package_header));
	if (pts<1)
		return;
	subject_package_header header = *pheader;
	if (m_sock->isWritable())
	{
		m_sock->write((const char *)fdata,header.data_length);
		//m_sock->flush();
	}
}
