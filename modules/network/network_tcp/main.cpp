#include "dialognettcp.h"
#include <QApplication>
#include <QVector>
#include <QMap>
#include <QLocale>
#include <QFile>
#include "cmdlineparser.h"
#include "tb_interface.h"
using namespace TASKBUS;
//#define OFFLINEDEBUG
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	init_client();

#ifdef OFFLINEDEBUG
	FILE * old_stdin, *old_stdout;
	auto ars = debug("D:/log/pid1290",&old_stdin,&old_stdout);
	const  cmdlineParser args (ars);
#else
	const cmdlineParser args (argc,argv);
#endif


	int ret = 0;

	//每个模块要响应 --information参数,打印自己的功能定义字符串。或者提供一个json文件。
	if (args.contains("information"))
	{
		QFile fp(":/json/network_tcp."+QLocale::system().name()+".json");
		if (fp.open(QIODevice::ReadOnly)==false)
		{
			fp.setFileName(":/json/network_tcp.exe.json");
			fp.open(QIODevice::ReadOnly);
		}
		if (fp.isOpen())
		{
			QByteArray arr = fp.readAll();
			arr.push_back('\0');
			puts(arr.constData());
			fflush(stdout);
		}
		ret = -1;
		return ret;
	}
	else
	{
		const QString para_address = QString::fromStdString(args.toString("address","127.0.0.1"));
		const int para_port = args.toInt("port",12800);
		const int para_mod = args.toInt("mod",0);
		const int para_hide = args.toInt("hide",0);
		const int instance = args.toInt("instance",0);
		const int para_in = args.toInt("input",0);
		const int para_out = args.toInt("output",0);


		DialogNetTCP w(instance);
		if (!para_hide)
			w.show();

		w.setNetPara(para_address,para_port,para_mod,para_in,para_out);
		if (args.contains("function"))
		{
			w.startWork();
		}

		ret = a.exec();
	}
	return ret;
}
