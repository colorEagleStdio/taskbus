#ifndef DIALOGNETUDP_H
#define DIALOGNETUDP_H

#include <QDialog>
#include <QSet>
#include <QStandardItemModel>
#include <QUdpSocket>
#include <QMap>
#include <QVector>
#include <thread>
#include <atomic>
#include "listen_thread.h"
#include "cmdlineparser.h"
#include "tb_interface.h"

namespace Ui {
	class DialogNetUDP;
}

class DialogNetUDP : public QDialog
{
	Q_OBJECT

public:
	explicit DialogNetUDP(const TASKBUS::cmdlineParser & cmd,QWidget *parent = 0);
	~DialogNetUDP();
	void startWork();
private:
	Ui::DialogNetUDP *ui = nullptr;
	QUdpSocket * m_sock = nullptr;
	reciv_thread * m_pRevThd = nullptr;
	const TASKBUS::cmdlineParser & m_cmd;
protected:
	std::thread * m_pBindThread = nullptr;
	std::thread * m_pWriteThread = nullptr;
	char (*m_bufdat)[65536] = nullptr;
	int *m_buflen = nullptr;
	const int m_bufsize = 1024;
	std::atomic<long long> m_pos_write;
	std::atomic<long long> m_pos_read;
signals:
	void sig_error(QString);
protected slots:
	void slot_new_taskpack(QByteArray);
	void slot_error(QString);
private slots:
	void on_pushButton_listerner_start_clicked();
	void on_lineEdit_sendAddr_textChanged(const QString &arg1);
	void on_lineEdit_sendPort_textChanged(const QString &arg1);
	void on_lineEdit_listenerAddr_textChanged(const QString &arg1);
	void on_lineEdit_listenerPort_textChanged(const QString &arg1);
	void on_checkBox_listener_bind_toggled(bool checked);
};

#endif // DIALOGNETUDP_H
