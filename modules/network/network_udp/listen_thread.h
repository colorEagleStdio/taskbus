#ifndef LISTENTHREAD_H
#define LISTENTHREAD_H
#include <QByteArray>
#include <QObject>
#include <QThread>
#include <atomic>
class reciv_thread: public QThread{
	Q_OBJECT
public:
	explicit reciv_thread(QObject * parent);
	void run() override;
signals:
	void new_package(QByteArray arr);
	void sig_quit();
};

extern std::atomic<bool> bfinished;

#endif
