#-------------------------------------------------
#
# Project created by QtCreator 2018-08-01T10:57:20
#
#-------------------------------------------------

QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += ../../../tb_interface
DESTDIR = $$OUT_PWD/../../../bin/modules
TARGET = network_p2p
TEMPLATE = app
include (../../../taskbusplatform.pri)
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
INCLUDEPATH += "../../../tb_interface"

SOURCES += \
	main.cpp \
	dialognetp2p.cpp \
	listen_thread.cpp

HEADERS += \
	dialognetp2p.h \
	listen_thread.h

FORMS += \
	dialognetp2p.ui

RESOURCES += \
	resource.qrc

#Documents Copy
QMAKE_POST_LINK += $${QMAKE_COPY_DIR} $$PWD/network_p2p.handbook $$DESTDIR/network_p2p.handbook
