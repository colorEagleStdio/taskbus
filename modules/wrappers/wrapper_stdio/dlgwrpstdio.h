#ifndef DLGWRPSTDIO_H
#define DLGWRPSTDIO_H

#include <QDialog>
#include <QProcess>
#include <QList>
#include "listen_thread.h"
namespace Ui {
	class DlgWrpStdio;
}

class DlgWrpStdio : public QDialog
{
	Q_OBJECT

public:
	explicit DlgWrpStdio(QWidget *parent = nullptr);
	~DlgWrpStdio();
	QStringList m_lstArgs;
	bool		isRunning ();
	QProcess *	proc(){return m_process;}
protected:
	void timerEvent(QTimerEvent *event);
public slots:
	bool cmd_start();
	bool cmd_stop();
	void run();
	QStringList split_cmd(QString cmdline);
private slots:
	void slot_newPackage(QByteArray arr);
	void slot_readyReadStandardOutput();
	void slot_readyReadStandardError();
	void slot_sended(qint64 );
	void slot_started();
	void slot_stopped();
	void slot_quit();
	void on_pushButton_start_clicked();
	void on_toolButton_path_clicked();
	void on_toolButton_workingDir_clicked();
private:
	Ui::DlgWrpStdio *ui;
	bool m_bHide = false;
	bool m_bPushStop = false;
	int m_timerId = -1;
	int m_nRestartCnt = -1;
	long long m_nRestartTimes = 0;
	listen_thread * m_plistenThd = nullptr;
	QProcess * m_process = nullptr;
	void loadIni();
	void saveIni();
};

#endif // DLGWRPSTDIO_H
