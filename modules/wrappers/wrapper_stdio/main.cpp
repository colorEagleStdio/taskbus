#include "dlgwrpstdio.h"
#include <QApplication>
#include <QFile>
#include "tb_interface.h"
#include "cmdlineparser.h"
using namespace TASKBUS;
const int OFFLINEDEBUG = 0;
cmdlineParser args;
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	TASKBUS::init_client();
	//解释命令行
	if (OFFLINEDEBUG==0)
		args.parser(argc,argv);
	else
	{
		FILE * old_stdin, *old_stdout;
		auto ars = debug("D:/log/pid1290",&old_stdin,&old_stdout);
		args.parser(ars);
	}

	if (args.contains("information"))
	{
		QFile fp(":/info.json");
		fp.open(QIODevice::ReadOnly);
		if (fp.isOpen())
		{
			QByteArray arr = fp.readAll();
			arr.push_back('\0');
			puts(arr.constData());
			fflush(stdout);
		}

		return 0;
	}
	else if (args.contains("function"))
	{
		DlgWrpStdio w;
		w.show();

		w.run();
		return a.exec();
	}
	else
	{
		DlgWrpStdio w;
		w.show();

		return a.exec();
	}
	return 0;
}
