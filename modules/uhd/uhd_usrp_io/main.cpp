﻿/*
 * Copyright 2015 Ettus Research LLC
 * Copyright 2018 Ettus Research, a National Instruments Company
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <QCoreApplication>
#include <QTextStream>
#include "cmdlineparser.h"
#include "tb_interface.h"
#include <QFile>
#include <QLocale>
#ifdef Q_OS_LINUX
#include <unistd.h>
#endif
using namespace TASKBUS;
using namespace std;
//#define OFFLINEDEBUG 0
int do_iio(const cmdlineParser & args);

int  main(int argc, char * argv[])
{
	QCoreApplication a(argc, argv);
	init_client();
#ifdef OFFLINEDEBUG
	FILE * old_stdin, *old_stdout;
	auto ars = debug("/home/user/codes/build-taskbus-qt6-Release/bin/debug/pid31171/",&old_stdin,&old_stdout);
	const  cmdlineParser args (ars);
#else
	const cmdlineParser args (argc,argv);
#endif

	int ret = 0;
	QTextStream stmerr(stderr);
	//每个模块要响应 --information参数,打印自己的功能定义字符串。或者提供一个json文件。
	if (args.contains("information"))
	{
		QFile fp(":/uhd_usrp_io."+QLocale::system().name()+".json");
		if (!fp.open(QIODevice::ReadOnly))
		{
			fp.setFileName(":/uhd_usrp_io.json");
			fp.open(QIODevice::ReadOnly);
		}
		if (fp.isOpen())
		{
			QByteArray arr = fp.readAll();
			arr.push_back('\0');
			puts(arr.constData());
			fflush(stdout);
		}
		ret = -1;
	}
	else if (args.contains("function"))//正常运行模式
	{
		fprintf(stderr,"Entering Loop...\n");
		//用于接收消息的线程
		ret = do_iio(args);
	}
	else
	{
		stmerr<<"Error:Function does not exits.\n";
		ret = -1;
	}
	return ret;




}
