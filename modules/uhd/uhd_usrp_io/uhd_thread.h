#ifndef UHD_THREAD_H
#define UHD_THREAD_H
#include <QThread>
#include <functional>
class uhd_io_thread: public QThread{
    Q_OBJECT
public:
    explicit uhd_io_thread (QObject * p);
    explicit uhd_io_thread (std::function<void (void)> runner,QObject * p);
    void setRunner(std::function<void (void)> r){ m_runner = r;}
public:
    void run() override;
private:
    std::function<void (void)> m_runner;
};


#endif // UHD_THREAD_H
