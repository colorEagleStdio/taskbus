QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

include (../../../taskbusplatform.pri)

INCLUDEPATH += ../../../tb_interface
DESTDIR = $$OUT_PWD/../../../bin/modules
SOURCES += \
	uhd_continous.cpp \
	uhd_io_continous.cpp \
	uhd_thread.cpp


RESOURCES += \
	resources.qrc
LIBS += -luhd
win32{
    INCLUDEPATH +="$(UHD_PKG_PATH)/include"
    LIBS += -L"$(UHD_PKG_PATH)/lib"
}

HEADERS += \
	uhd_thread.h

