#include "uhd_thread.h"

uhd_io_thread::uhd_io_thread (std::function<void (void)> runner,QObject * p)
    :QThread(p)
    ,m_runner(runner)

{

}
uhd_io_thread::uhd_io_thread (QObject * p)
    :QThread(p)

{

}

void uhd_io_thread::run()
{
    if (m_runner)
        m_runner();
}
