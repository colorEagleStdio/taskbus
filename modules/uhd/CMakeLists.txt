cmake_minimum_required(VERSION 3.10)

if (TB_UHD)
	add_subdirectory(uhd_usrp_continous)
	add_subdirectory(uhd_usrp_io)
endif()
