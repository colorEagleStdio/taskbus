/*!
  * taskNode用来管理各个功能对应的进程。taskNode实例与一个进程一一对应，负责直接管理
  * 进程启动、终止、数据组合与吞吐。该类从QObject派生，将在taskProject的管理下，
  * 运行在线程池中。
  * Tasknode is used to manage the processes that correspond to each Function.
  * The Tasknode instance corresponds to a process one by one, which is
  * responsible for directly managing the process initiation, termination,
  * data combination, and throughput. The class is derived from Qobject and
  * will run in the thread pool under taskproject management.
  * @author cestdio-studio, 2016-09
  */
#ifndef TASKNODE_H
#define TASKNODE_H

#include <QObject>
#include "taskcell.h"
#include <QProcess>
#include <QList>
#include <QByteArrayList>
#include <QFile>
#include <QMutex>
#include <QEvent>
class taskCell;

class taskProject;

/*!
 * \brief The taskNode class 管理一个构件进程的类
 * 这个类包含基本的进程启动终止、管道维护
 * A clapush_backss that manages a component process. This class contains basic process
 *  start-up termination, pipeline maintenance
 */
class taskNode : public QObject
{
	Q_OBJECT
public:
	explicit taskNode( QObject *parent = nullptr);
	~taskNode();
signals:
	//进程消息 Process messages
	void sig_pro_started();
	void sig_pro_stopped(int exitCode, QProcess::ExitStatus exitStatus);
	void sig_new_command(QMap<QString, QVariant> cmd);
	void sig_new_errmsg(QByteArrayList);
	void sig_iostat(qint64 pid,quint64 pr,quint64 ps,quint64 br, quint64 bs);
public slots:
	void setDebug(bool bdbg){m_bDebug = bdbg;}
	bool cmd_start(QObject * node,QString cmd, QStringList paras);
	bool cmd_stop(QObject * node);
	bool cmd_sendcmd(QMap<QString,QVariant> cmd, QSet<QString> destins);
private slots:
	void slot_readyReadStandardOutput();
	void slot_readyReadStandardError();
	void slot_sended(qint64 );
	void slot_started( );
	void slot_stopped();
private:
	QProcess * m_process = nullptr;
	taskProject * m_currPrj = nullptr;
	//1.进程缓存 stdin
	QVector<QByteArray> m_array_stdin;
	QVector<qsizetype> m_size_stdin;
	QVector<int> m_status_stdin;
	QVector<QAtomicInteger<qsizetype> > m_cnt_stdin;
	unsigned long long m_pos_stdin = 0;
	const qsizetype m_bufsize_stdin = 65536;
	qsizetype m_bufsize_adjust = 65536;
public:
	inline QByteArray & get_stdin_array(qsizetype pos) {return m_array_stdin[pos];}
	inline qsizetype & get_stdin_size(qsizetype pos){return m_size_stdin[pos];}
	inline QAtomicInteger<qsizetype> & get_stdin_cnt(qsizetype pos){return m_cnt_stdin[pos];}
private:
	//2.stderr
	QByteArrayList m_arr_Strerr;
	bool m_bDebug = false;
	//Flush buffered messages, emit signals.
	void flush_from_stderr();
	QString dbgdir();
	void log_package(bool fromStdOut,char * pkg, qsizetype sz);
	void flush_write();
	QFile m_dbgfile_stdin;
	QFile m_dbgfile_stdout;
	QFile m_dbgfile_stderr;
	QString m_uuid;
	//Call these functions to emit message and packs with bufferring approach.
	void emit_message(QByteArray a);
	//3.Stdout
private:
	QMutex m_mtx_queue;
	QByteArrayList m_write_cmd;
	QList<taskNode *> m_write_queue;
	QList<qsizetype> m_write_pos;
	//other methods.
public:
	void setCurrentProject(taskProject * p);
	bool enqueue_write(taskNode * node, qsizetype pos);
protected:
	int  m_nBatchTime = 20;
	int  m_nBp_TimerID = -1;
	static QEvent::Type	 m_nPackEvent ;
	taskCell * m_pCell = nullptr;
	void timerEvent(QTimerEvent *event) override;
	void customEvent(QEvent * evt) override;
	//statistic
protected:
	quint64 m_spackage_recieved = 0;
	quint64 m_spackage_sent = 0;
	quint64 m_sbytes_recieved = 0;
	quint64 m_sbytes_sent = 0;
	QByteArray m_arrStdErr;
	bool m_bCmdStop = false;
	int  m_nCmdRestart = -1;
public:
	bool		isRunning ();
	QString		uuid()const {return m_uuid;}
	bool		isDebug() const {return m_bDebug;}
	QProcess *	proc(){return m_process;}
	void		bindCell(taskCell * t){m_pCell = t;}
	taskCell *	cell(){return m_pCell;}
	quint64 st_pack_recieved() const {return m_spackage_recieved;}
	quint64 st_pack_sended() const {return m_spackage_sent;}
	quint64 st_bytes_recieved() const {return m_sbytes_recieved;}
	quint64 st_bytes_sended() const {return m_sbytes_sent;}
};

#endif // TASKNODE_H
