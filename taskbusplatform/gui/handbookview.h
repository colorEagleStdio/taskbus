#ifndef HANDBOOKVIEW_H
#define HANDBOOKVIEW_H

#include <QWidget>

namespace Ui {
	class HandbookView;
}

class HandbookView : public QWidget
{
	Q_OBJECT

public:
	explicit HandbookView(QWidget *parent = nullptr);
	~HandbookView();
public:
	void setUrl(QString urlstr, QString FailedString = "");
private slots:
	void on_lineEdit_url_returnPressed();

private:
	Ui::HandbookView *ui;
};

#endif // HANDBOOKVIEW_H
