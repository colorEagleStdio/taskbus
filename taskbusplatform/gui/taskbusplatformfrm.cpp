﻿#include "taskbusplatformfrm.h"
#include "ui_taskbusplatformfrm.h"
#include <QCloseEvent>
#include <QDir>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QMdiSubWindow>
#include <QSettings>
#include <QDateTime>
#include "dlgsettings.h"
#include "pdesignerview.h"
#include "handbookview.h"
#include "watchdog/tbwatchdog.h"
#include "watchdog/watchmemmodule.h"
#include "dlgabout.h"

int taskBusPlatformFrm::m_doc_ins = 0;

taskBusPlatformFrm::taskBusPlatformFrm(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::taskBus),
	m_pTrayIcon(new QSystemTrayIcon(this)),
	m_pMsgModel(new QStandardItemModel(this)),
	m_pCmdViewModel(new QStandardItemModel(this)),
	m_pClassModel(new QStandardItemModel(this))
{
	ui->setupUi(this);
	setCentralWidget(ui->mdiArea);
	//全部模块 Create Module for All taskModules
	m_pRefModule = m_toolModules[tr("All")] = new taskModule(true,this);
	ui->listView_modules->setModel(m_toolModules[tr("All")]);

	//类别 ComboBox for classes
	m_pClassModel->appendRow(new QStandardItem(tr("All")));
	ui->comboBox_class->setModel(m_pClassModel);
	//消息 message module
	ui->listView_messages->setModel(m_pMsgModel);
	ui->listView_commands->setModel(m_pCmdViewModel);
	//状态栏 status bar
	m_pStatus = new QLabel(this);
	statusBar()->addWidget(m_pStatus);

	m_nTmid = startTimer(1000);
	tabifyDockWidget(ui->dockWidget_props,ui->dockWidget_cmd);
	tabifyDockWidget(ui->dockWidget_props,ui->dockWidget_watch);
	tabifyDockWidget(ui->dockWidget_props,ui->dockWidget_message);
	m_iconTray[0].addFile(":/taskBus/images/ticon1.png");
	m_iconTray[1].addFile(":/taskBus/images/ticon2.png");
	m_pTrayIcon->setIcon(m_iconTray[0]);
	m_pTrayIcon->show();

	QMenu * me = new QMenu(this);
	me->addAction(ui->actionhideWindow);
	me->addAction(ui->action_About);
	m_pTrayIcon->setContextMenu(me);
	connect(m_pTrayIcon,&QSystemTrayIcon::activated,this,&taskBusPlatformFrm::slot_traymessage);
	ui->menu_View->insertAction(ui->actionhideWindow,ui->dockWidget_message->toggleViewAction());
	ui->menu_View->insertAction(ui->actionhideWindow,ui->dockWidget_modules->toggleViewAction());
	ui->menu_View->insertAction(ui->actionhideWindow,ui->dockWidget_props->toggleViewAction());
	ui->menu_View->insertAction(ui->actionhideWindow,ui->dockWidget_watch->toggleViewAction());

	m_bShowCmds = false;
}

taskBusPlatformFrm::~taskBusPlatformFrm()
{
	QList<QMdiSubWindow *> lst = ui->mdiArea->subWindowList();
	foreach (QMdiSubWindow * sub, lst)
	{
		PDesignerView * dv = qobject_cast<PDesignerView *>(sub->widget());
		if (dv)
			dv->stop();
	}
	delete ui;
}

void taskBusPlatformFrm::slot_traymessage(QSystemTrayIcon::ActivationReason r)
{
	switch (r)
	{
	case QSystemTrayIcon::DoubleClick:
		ui->actionhideWindow->setChecked(false);
		break;
	default:
		break;
	}
}

void taskBusPlatformFrm::timerEvent(QTimerEvent *event)
{
	static int pp = 0;
	if (m_nTmid==event->timerId())
	{
		extern QAtomicInteger<quint64>  g_totalrev, g_totalsent;
		QString s = QString("down %1Bps, up %2Bps").arg(WatchMemModule::dynamic_text(g_totalrev)).arg(WatchMemModule::dynamic_text(g_totalsent));

		m_pStatus->setText(s);
		if (g_totalrev>0 || g_totalrev>0)
		{
			++pp;
			m_pTrayIcon->setIcon(m_iconTray[pp%2]);
		}
		else if (pp)
		{
			pp = 0;
			m_pTrayIcon->setIcon(m_iconTray[0]);
		}
		g_totalsent = 0;
		g_totalrev = 0;
	}
}

QString taskBusPlatformFrm::inifile()
{
	return QCoreApplication::applicationFilePath()+".ini";
}

void taskBusPlatformFrm::slot_showPropModel(QObject * objModel)
{
	taskModule * pm = qobject_cast<taskModule *>(objModel);
	if (pm)
	{
		ui->treeView_props->setModel(pm);
		ui->treeView_props->collapseAll();
		ui->treeView_props->expand(pm->index(0,0));
		ui->treeView_props->expand(pm->paraIndex());
		ui->treeView_props->scrollToBottom();
		//ui->treeView_props->expandAll();
	}
	else
		ui->treeView_props->setModel(0);
}

void taskBusPlatformFrm::on_action_About_triggered()
{
	show();
	DlgAbout about(this);
	if (about.exec()==QDialog::Accepted)
		QApplication::aboutQt();
}

void taskBusPlatformFrm::on_action_Start_project_triggered()
{
	QMdiSubWindow * sub = ui->mdiArea->activeSubWindow();
	if (sub)
	{
		PDesignerView * dv = qobject_cast<PDesignerView *>(sub->widget());
		if (dv)
		{
			dv->run();
			on_mdiArea_subWindowActivated(sub);
		}
	}
}


void taskBusPlatformFrm::on_action_stop_project_triggered()
{
	QMdiSubWindow * sub = ui->mdiArea->activeSubWindow();
	if (sub)
	{
		PDesignerView * dv = qobject_cast<PDesignerView *>(sub->widget());
		if (dv)
		{
			dv->stop();
			on_mdiArea_subWindowActivated(sub);
		}
	}

}


void taskBusPlatformFrm::on_mdiArea_subWindowActivated(QMdiSubWindow *arg1)
{
	if (!arg1)
	{
		ui->action_Start_project->setEnabled(false);
		ui->action_stop_project->setEnabled(false);
		ui->action_Save_Project->setEnabled(false);
		ui->dockWidget_props->setEnabled(false);
		ui->treeView_props->setModel(0);
		return;
	}
	else
	{
		ui->action_Save_Project->setEnabled(true);
	}
	PDesignerView * dv = qobject_cast<PDesignerView *>(arg1->widget());
	if (dv)
	{
		int noden = dv->selectedNode();
		if (noden<0||noden>=dv->project()->vec_cells().size())
			ui->treeView_props->setModel(0);
		else
		{
			taskCell * c = dv->project()->vec_cells()[noden];
			taskModule * m = dynamic_cast<taskModule *>(c);
			if (m!=ui->treeView_props->model() && m)
			{
				ui->treeView_props->setModel(m);
				ui->treeView_props->collapseAll();
				ui->treeView_props->expand(m->index(0,0));
				ui->treeView_props->expand(m->paraIndex());
				ui->treeView_props->scrollToBottom();
			}
			else if (!m)
				ui->treeView_props->setModel(0);
		}

		ui->action_Start_project->setEnabled(!dv->is_running());
		ui->action_stop_project->setEnabled(dv->is_running());
		ui->dockWidget_props->setEnabled(!dv->is_running());
		dv->setEnabled(!dv->is_running());
	}
	else
	{
		ui->action_Start_project->setEnabled(false);
		ui->action_stop_project->setEnabled(false);
		ui->action_Save_Project->setEnabled(false);
		ui->dockWidget_props->setEnabled(false);
		ui->treeView_props->setModel(0);
		return;
	}
}

void taskBusPlatformFrm::on_comboBox_class_currentIndexChanged(int index)
{
	if (index>=0 && index<m_pClassModel->rowCount())
	{
		QString name = m_pClassModel->data(m_pClassModel->index(index,0)).toString();
		if (m_toolModules.contains(name))
			ui->listView_modules->setModel(m_toolModules[name]);
	}
}

void taskBusPlatformFrm::closeEvent(QCloseEvent * event)
{
	bool bModified = false;
	bool bRunning = false;
	QStringList lsN = m_activePagesFileName.keys();
	foreach (QString k, lsN)
	{
		QWidget * w = m_activePagesFileName[k]->widget();
		if (w)
		{
			PDesignerView * v = qobject_cast<PDesignerView*>(w);
			if(v)
			{
				bModified = bModified || v->modified();
				bRunning = bRunning || v->project()->isRunning();
			}
		}
	}
	if (bRunning)
	{
		QMessageBox::information(this,tr("Still running"),tr("Project is still running, please stop all projects first."),QMessageBox::Ok);
		event->ignore();
		return;
	}
	if (bModified)
	{
		if (QMessageBox::information(this,tr("Close without saving?"),tr("Project has been modified, Close it anyway?"),
									 QMessageBox::Ok,
									 QMessageBox::Cancel)!=QMessageBox::Ok)
		{
			event->ignore();
			return;
		}
	}
	event->accept();
	this->m_pTrayIcon->hide();
}

void taskBusPlatformFrm::on_actionhideWindow_toggled(bool arg1)
{
	if (arg1)
		hide();
	else
		show();
}



void taskBusPlatformFrm::on_listView_modules_doubleClicked(const QModelIndex &index)
{
	if (ui->listView_modules->model())
	{
		taskModule * mod = qobject_cast<taskModule *>(ui->listView_modules->model());
		if (!mod)
			return;
		int r = index.row();
		QStringList functions = mod->function_names();
		if (r<0 || r>=functions.size())
			return;
		QString func = functions.at(r);
		QString exe = mod->function_exec(func);
		load_doucment(func,exe);

	}

}

void taskBusPlatformFrm::on_checkBox_showCmd_stateChanged(int /*arg1*/)
{
	m_bShowCmds = ui->checkBox_showCmd->isChecked();
}
void taskBusPlatformFrm::slot_cmd_show(QMap<QString,QVariant> cmd)
{
	if (m_bShowCmds==false)
		return;
	const int keepRows = 128;
	QString prefix = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
	QString str = taskCell::map_to_string(cmd);
	m_pCmdViewModel->appendRow(new QStandardItem(prefix+">"+str));
	if (m_pCmdViewModel->rowCount()>keepRows)
		m_pCmdViewModel->removeRows(0,m_pCmdViewModel->rowCount()-keepRows);
	ui->listView_commands->scrollToBottom();
}

void  taskBusPlatformFrm::load_doucment(QString func,QString exe)
{
	//Open handbook
	QFileInfo info(exe);
	QString baseName = info.absolutePath()+"/"+
			info.completeBaseName();
	QString urlstr;
	QString FailedString;
	FailedString += exe+".md/.html/.txt" + "\n\n";
	FailedString += baseName+".md/.html/.txt" + "\n\n";
	FailedString += baseName+".handbook/" + info.completeBaseName() + ".md/.html/.txt\n\n";
	if (QFileInfo::exists(exe+".html"))
		urlstr = exe+".html";
	else if (QFileInfo::exists(baseName+".html"))
		urlstr = baseName+".html";
	else if (QFileInfo::exists(baseName+".handbook/" + info.completeBaseName() + ".html"))
		urlstr = baseName+".handbook/" + info.completeBaseName() + ".html";
	else if (QFileInfo::exists(exe+".md"))
		urlstr = exe+".md";
	else if (QFileInfo::exists(baseName+".md"))
		urlstr = baseName+".md";
	else if (QFileInfo::exists(baseName+".handbook/" + info.completeBaseName() + ".md"))
		urlstr = baseName+".handbook/" + info.completeBaseName() + ".md";
	else if (QFileInfo::exists(exe+".txt"))
		urlstr = exe+".txt";
	else if (QFileInfo::exists(baseName+".txt"))
		urlstr = baseName+".txt";
	else if (QFileInfo::exists(baseName+".handbook/" + info.completeBaseName() + ".txt"))
		urlstr = baseName+".handbook/" + info.completeBaseName() + ".txt";

	QList<QMdiSubWindow *> AllSubWnds = ui->mdiArea->subWindowList();
	QString title = tr("DOC:") + func;
	//Find existing window
	QMdiSubWindow * wnd = nullptr;
	foreach (QMdiSubWindow * w, AllSubWnds)
	{
		if (w->windowTitle()==title)
		{
			wnd = w;
			ui->mdiArea->setActiveSubWindow(w);
			break;
		}
	}
	if (!wnd )
	{
		HandbookView * view  = new HandbookView(this);
		if (view)
		{
			view->setWindowTitle(title);
			wnd = ui->mdiArea->addSubWindow(view);
			view->show();
			view->setUrl(urlstr,FailedString);
			wnd->setWindowTitle(title);
		}
	}

}

void taskBusPlatformFrm::on_action_Help_triggered()
{
	load_doucment("taskBus",QCoreApplication::applicationFilePath());
}


void taskBusPlatformFrm::on_actionSettings_triggered()
{
	DlgSettings setts;
	setts.exec();
}

void taskBusPlatformFrm::openProject(QString filename, bool autorun)
{
	slot_openprj(filename);
	for (int i=0;i<100;++i)
	{
		QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
		QThread::msleep(10);
	}
	if (autorun)
		on_action_Start_project_triggered();
	for (int i=0;i<100;++i)
	{
		QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
		QThread::msleep(10);
	}
}

void taskBusPlatformFrm::hideMainFrame()
{
	if (!ui->actionhideWindow->isChecked())
		ui->actionhideWindow->setChecked(true);

}

