#include "dlgsettings.h"
#include "ui_dlgsettings.h"
#include <QDir>
#include <QMessageBox>
#include <QFileDialog>
DlgSettings::DlgSettings(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DlgSettings)
{
	ui->setupUi(this);
	QSettings settings(QCoreApplication::applicationFilePath()+".ini",QSettings::IniFormat);
	QString plgPath = settings.value("settings/QT_PLUGIN_PATH",qgetenv("QT_PLUGIN_PATH")).toString();
	QString uhdPath = settings.value("settings/UHD_PKG_PATH",qgetenv("UHD_PKG_PATH")).toString();
	ui->lineEdit_QT_PLUGIN_PATH->setText(plgPath);
	ui->lineEdit_UHD_PKG_PATH->setText(uhdPath);
#ifdef Q_OS_LINUX
	ui->spinBox_nice->setRange(-20,20);
	ui->spinBox_nice->setValue(settings.value("settings/nice",-5).toInt());
	ui->lineEdit_QT_PLUGIN_PATH->setEnabled(false);
	ui->lineEdit_UHD_PKG_PATH->setEnabled(false);
#else
	ui->spinBox_nice->setRange(0,5);
	ui->spinBox_nice->setValue(settings.value("settings/nice",5).toInt());
#endif
	ui->spinBox_batchtime->setValue(settings.value("settings/batchtime",0).toInt());
	ui->spinBox_ringCache->setValue(settings.value("settings/ringcache",128).toInt());


	settings.setValue("settings/QT_PLUGIN_PATH",ui->lineEdit_QT_PLUGIN_PATH->text());
	settings.setValue("settings/UHD_PKG_PATH",ui->lineEdit_UHD_PKG_PATH->text());
	settings.setValue("settings/nice",ui->spinBox_nice->value());
	settings.setValue("settings/batchtime",ui->spinBox_batchtime->value());
	settings.setValue("settings/ringcache",ui->spinBox_ringCache->value());

}

DlgSettings::~DlgSettings()
{
	delete ui;
}

void DlgSettings::on_buttonBox_accepted()
{
	QSettings settings(QCoreApplication::applicationFilePath()+".ini",QSettings::IniFormat);

	settings.setValue("settings/QT_PLUGIN_PATH",ui->lineEdit_QT_PLUGIN_PATH->text());
	settings.setValue("settings/UHD_PKG_PATH",ui->lineEdit_UHD_PKG_PATH->text());
	settings.setValue("settings/nice",ui->spinBox_nice->value());
	settings.setValue("settings/batchtime",ui->spinBox_batchtime->value());
	settings.setValue("settings/ringcache",ui->spinBox_ringCache->value());

	QMessageBox::information(this,tr("Need restart"),tr("New config will be loaded at next startup."));

}


void DlgSettings::on_toolButton_UHD_PKG_PATH_clicked()
{
#ifdef Q_OS_LINUX
	QMessageBox::information(this,tr("Only Windows"),tr("for Linux please install depends from package manage ."));
	return;
#endif
	QDir dir(QCoreApplication::applicationDirPath());
	QString ps = QFileDialog::getExistingDirectory(this,tr("UHD_PKG_PATH"),ui->lineEdit_UHD_PKG_PATH->text());
	if (ps.length())
	{
		QString rel =dir.relativeFilePath(ps);
		if (rel.length()<ps.length())
		{
			ps = rel;
		}
		ui->lineEdit_UHD_PKG_PATH->setText(ps);
	}
}


void DlgSettings::on_toolButton_QT_PLUGIN_PATH_clicked()
{
#ifdef Q_OS_LINUX
	QMessageBox::information(this,tr("Only Windows"),tr("for Linux please install depends from package manage ."));
	return;
#endif
	QDir dir(QCoreApplication::applicationDirPath());
	QString ps = QFileDialog::getExistingDirectory(this,tr("QT_PLUGIN_PATH"),ui->lineEdit_QT_PLUGIN_PATH->text());
	if (ps.length())
	{
		QString rel =dir.relativeFilePath(ps);
		if (rel.length()<ps.length())
		{
			ps = rel;
		}
		ui->lineEdit_QT_PLUGIN_PATH->setText(ps);
	}
}

