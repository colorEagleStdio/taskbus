#include "handbookview.h"
#include "ui_handbookview.h"

HandbookView::HandbookView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::HandbookView)
{
	ui->setupUi(this);
	connect (ui->textBrowser_help, &QTextBrowser::backwardAvailable,ui->pushButton_backward,&QPushButton::setEnabled);
	connect (ui->textBrowser_help, &QTextBrowser::forwardAvailable,ui->pushButton_forward,&QPushButton::setEnabled);
	connect (ui->pushButton_forward,&QPushButton::pressed,ui->textBrowser_help, &QTextBrowser::forward);
	connect (ui->pushButton_backward,&QPushButton::pressed,ui->textBrowser_help, &QTextBrowser::backward);
	connect (ui->textBrowser_help, &QTextBrowser::sourceChanged,[&](QUrl u){
		ui->lineEdit_url->setText(u.toDisplayString());
	});

}

HandbookView::~HandbookView()
{
	delete ui;
}
void HandbookView::setUrl(QString urlstr,QString FailedString)
{
	if (urlstr.length())
	{
		QUrl u = QUrl::fromLocalFile(urlstr);
		ui->textBrowser_help->setSource(u);
	}
	else
	{
		ui->textBrowser_help->setMarkdown(
					tr("# Documents Does NOT Exist\r\n\r\n"
					"You can write documents with markdown in any place below:\r\n\r\n")
					+FailedString
					);
	}



}

void HandbookView::on_lineEdit_url_returnPressed()
{
	QUrl u = QUrl(ui->lineEdit_url->text());
	ui->textBrowser_help->setSource(u);
}

