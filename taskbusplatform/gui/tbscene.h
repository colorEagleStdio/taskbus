#ifndef TBSCENE_H
#define TBSCENE_H

#include <QGraphicsScene>
#include <QGraphicsLineItem>
class TBScene : public QGraphicsScene
{
	Q_OBJECT
public:
	TBScene(QObject *parent = nullptr);
	TBScene(const QRectF &sceneRect, QObject *parent = nullptr);
	TBScene(qreal x, qreal y, qreal width, qreal height, QObject *parent = nullptr);
protected:
	void timerEvent(QTimerEvent * e) override;
	void mouseMoveEvent(QGraphicsSceneMouseEvent * e) override;
private:
	QGraphicsLineItem * m_pinLine = nullptr;
	int m_nTimerID = -1;
};

#endif // TBSCENE_H
