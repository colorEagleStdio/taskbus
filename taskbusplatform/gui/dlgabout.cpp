﻿#include "dlgabout.h"
#include "ui_dlgabout.h"
#include <QUrl>
#include <QDateTime>
#include "../taskbus_version.h"
DlgAbout::DlgAbout(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DlgAbout)
{
	ui->setupUi(this);
	ui->textBrowser_About->
			setText(tr("Taskbus is a cross-platform"
			 " multi-process cooperation framework for non-professional "
			 "developers, with four features of process based, "
			 "language independent, compiler independent, and architecture Independent.\n"
			 "by Colored Eagle Studio, 2016~%1").arg(QDateTime::currentDateTime().date().year()));
	ui->label_version->setText(QString("V%1").arg(TASKBUS_VERSION));
}

DlgAbout::~DlgAbout()
{
	delete ui;
}

void DlgAbout::on_pushButton_Next_clicked()
{
	this->accept();
}

void DlgAbout::on_pushButton_Close_clicked()
{
	this->reject();
}
