#include "tbscene.h"
#include "tgraphicstaskitem.h"
#include <QGraphicsSceneMouseEvent>
TBScene::TBScene(QObject *parent)
	: QGraphicsScene{parent}
	, m_pinLine(new QGraphicsLineItem())
{
	addItem(m_pinLine);
	m_nTimerID = startTimer(500);
}
TBScene::TBScene(const QRectF &sceneRect, QObject *parent )
	:QGraphicsScene(sceneRect,parent)
	, m_pinLine(new QGraphicsLineItem())
{
	addItem(m_pinLine);
	m_nTimerID = startTimer(500);
}
TBScene::TBScene(qreal x, qreal y, qreal width, qreal height, QObject *parent)
	:QGraphicsScene(x,y,width,height,parent)
	, m_pinLine(new QGraphicsLineItem())
{
	addItem(m_pinLine);
	m_nTimerID = startTimer(500);
}

void TBScene::mouseMoveEvent(QGraphicsSceneMouseEvent * e)
{
	QGraphicsScene::mouseMoveEvent(e);
	if (TGraphicsTaskItem::m_pinList.size()==1)
	{
		m_pinLine->setLine(TGraphicsTaskItem::m_pinPoint.x(),TGraphicsTaskItem::m_pinPoint.y(),
						   e->lastScenePos().x(),e->lastScenePos().y());
	}
}
void TBScene::timerEvent(QTimerEvent * e)
{
	static unsigned long long colori = 0;
	if (e->timerId()==m_nTimerID)
	{
		if (TGraphicsTaskItem::m_pinList.size()==1)
		{
			QPen pen(Qt::DotLine);
			pen.setWidth(2);
			pen.setColor(QColor(++colori * 49999473677 % 0xffffff));
			m_pinLine->setPen(QPen(pen));
		}
		else if (m_pinLine->line().x1()||m_pinLine->line().x2())
		{
			m_pinLine->setLine(0,0,0,0);
		}
	}
}
