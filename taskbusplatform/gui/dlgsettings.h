#ifndef DLGSETTINGS_H
#define DLGSETTINGS_H

#include <QDialog>
#include <QSettings>
namespace Ui {
	class DlgSettings;
}

class DlgSettings : public QDialog
{
	Q_OBJECT

public:
	explicit DlgSettings(QWidget *parent = nullptr);
	~DlgSettings();

private slots:
	void on_buttonBox_accepted();

	void on_toolButton_UHD_PKG_PATH_clicked();

	void on_toolButton_QT_PLUGIN_PATH_clicked();

private:
	Ui::DlgSettings *ui;
};

#endif // DLGSETTINGS_H
