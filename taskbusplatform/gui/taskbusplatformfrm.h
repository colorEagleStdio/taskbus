﻿#ifndef PROGRESSBUSPLATFORMFRM_H
#define PROGRESSBUSPLATFORMFRM_H
#include "taskmodule.h"
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QStandardItemModel>
#include <QLabel>
#include <QMap>
#include <QSystemTrayIcon>
namespace Ui {
	class taskBus;
}

class taskBusPlatformFrm : public QMainWindow
{
	Q_OBJECT

public:
	explicit taskBusPlatformFrm(QWidget *parent = 0);
	~taskBusPlatformFrm() override;
	void load_mod_def(QString mod_file);
	void save_mod_def(QString mod_file);
	void unregProject(QString fm);
	taskModule * refModule(){return m_pRefModule;}
	void openProject(QString filename, bool autorun);
	void hideMainFrame();
private slots:
	void slot_showPropModel(QObject * objModel);
	void slot_showMsg(QStringList namestr,QByteArrayList strMessages);
	void slot_openprj(QString);
	void slot_projstarted();
	void slot_projstopped();
	void slot_traymessage(QSystemTrayIcon::ActivationReason r);
	void slot_cmd_show(QMap<QString,QVariant>);
	void on_action_New_Project_triggered();
	void on_action_About_triggered();
	void on_action_Start_project_triggered();
	void on_action_Save_Project_triggered();
	void on_action_stop_project_triggered();
	void on_action_Open_Project_triggered();
	void on_mdiArea_subWindowActivated(QMdiSubWindow *arg1);
	void on_comboBox_class_currentIndexChanged(int index);
	void on_actionhideWindow_toggled(bool arg1);
	void on_action_Save_Project_As_triggered();
	void on_listView_modules_doubleClicked(const QModelIndex &index);
	void on_checkBox_showCmd_stateChanged(int arg1);
	void on_action_Help_triggered();
	void on_actionSettings_triggered();
	void on_toolButton_ModDel_clicked();
	void on_toolButton_modSave_clicked();
	void on_toolButton_ModLoad_clicked();
	void on_toolButton_addMod_clicked();

signals:
	void showSplash(QString msg,Qt::Alignment, QColor);
	void hideSplash();
protected:
	void timerEvent(QTimerEvent *event) override;
	void closeEvent(QCloseEvent * event) override;
	void load_modules(QStringList lstNames);
	void load_doucment(QString func,QString exeFile);
private:
	Ui::taskBus *ui;
	QSystemTrayIcon * m_pTrayIcon = nullptr;
	QIcon m_iconTray[2];
	QLabel * m_pStatus;
	int m_nTmid = -1;
	QMap<QString,taskModule *> m_toolModules;
	taskModule * m_pRefModule = nullptr;
	QMap<QString,QMdiSubWindow *> m_activePagesFileName;
	QStandardItemModel * m_pMsgModel;
	QStandardItemModel * m_pCmdViewModel;
	QStandardItemModel * m_pClassModel;
	QString inifile();
	static int m_doc_ins;
	bool m_bShowCmds;
};

#endif // PROGRESSBUSPLATFORMFRM_H
