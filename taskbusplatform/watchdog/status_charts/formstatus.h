#ifndef FORMSTATUS_H
#define FORMSTATUS_H

#include <QWidget>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QDateTimeAxis>
#include "watchdog/watchmemmodule.h"
namespace Ui {
	class FormStatus;
}

class FormStatus : public QWidget
{
	Q_OBJECT

public:
	explicit FormStatus(QWidget *parent = nullptr);
	~FormStatus() override;
	WatchMemModule * wmod() {return m_watchModule;}
	void clearMap(){m_chart_serials.clear();}
protected:
	void timerEvent(QTimerEvent *event) override;
private:
	Ui::FormStatus *ui;
	int m_nTmid = -1;
	WatchMemModule * m_watchModule = nullptr;
	QDateTimeAxis * m_DateAxix = nullptr;
	QValueAxis * m_DateAxiy = nullptr;
	QMap<qint64, QLineSeries *> m_chart_serials;
	void update_charts();
};

#endif // FORMSTATUS_H
