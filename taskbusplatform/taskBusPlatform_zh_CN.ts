<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DlgAbout</name>
    <message>
        <location filename="gui/dlgabout.ui" line="14"/>
        <source>About taskBus</source>
        <translation>关于本软件</translation>
    </message>
    <message>
        <source>Main &amp;page</source>
        <translation type="vanished">主页（&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Documents</source>
        <translation type="vanished">文档（&amp;D)</translation>
    </message>
    <message>
        <location filename="gui/dlgabout.ui" line="82"/>
        <source>&amp;About Qt</source>
        <translation>关于Qt（&amp;A)</translation>
    </message>
    <message>
        <location filename="gui/dlgabout.ui" line="89"/>
        <source>&amp;Close</source>
        <translation>关闭（&amp;C)</translation>
    </message>
    <message>
        <location filename="gui/dlgabout.cpp" line="11"/>
        <source>Taskbus is a cross-platform multi-process cooperation framework for non-professional developers, with four features of process based, language independent, compiler independent, and architecture Independent.
by Colored Eagle Studio, 2016~%1</source>
        <translation>taskBus 是一个为非计算机专业开发者进行敏捷开发准备的跨平台多进程合作框架。它具有四个特点：基于多进程、语言无关、编译器无关、架构无关。
彩鹰工作室，2016~%1</translation>
    </message>
</context>
<context>
    <name>DlgSettings</name>
    <message>
        <location filename="gui/dlgsettings.ui" line="17"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="23"/>
        <location filename="gui/dlgsettings.cpp" line="59"/>
        <source>UHD_PKG_PATH</source>
        <translation>UHD_PKG_PATH</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="30"/>
        <source>UHD_PKG_PATH Generally, it is set to the ettus UHD driver installation folder, which contains bin, include, lib, share and other subfolders. The share folder contains the UHD FPGA image.</source>
        <translation>UHD_PKG_PATH 一般设置为Ettus UHD库的安装路径。这个路径里能看到bin,lib,include,share等文件夹。其中，最大的文件夹是share/uhd，里面有UHD设备的FPGA门镜像。</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="37"/>
        <location filename="gui/dlgsettings.ui" line="58"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="44"/>
        <location filename="gui/dlgsettings.cpp" line="79"/>
        <source>QT_PLUGIN_PATH</source>
        <translation>QT_PLUGIN_PATH</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="51"/>
        <source>QT_PLUGIN_PATH This is usually set to the path where the Qt plugin folder is located. There are plugins such as platforms, styles, etc. in this path.</source>
        <translation>QT_PLUGIN_PATH 是Qt版本对应的插件文件夹。在这个文件夹里，应该包含platforms, imageformats, sql等插件文件夹。</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="65"/>
        <source>BATCH_TIME</source>
        <translation>BATCH_TIME</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="72"/>
        <source>batchTime controls the batch processing of signals and slots. For scenarios that require low latency, set to 0. Scenes that require high throughput are set to be larger.</source>
        <translation>BATCH_TIME控制了信号与槽/事件的周转效率。对低延迟的场景，应该设置为0. 对高吞吐的场景，可以设置较大的值。</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="82"/>
        <source>NICE</source>
        <translation>NICE</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.ui" line="89"/>
        <source>nice controls the process priority. Under Windows, 0 is the IDLE and 5 is the highest. Under Linux, -20 is the highest, and 20 is idle.</source>
        <translation>NICE控制主吞吐进程的优先级。在Windows下，0是空闲（最低），5是实时（最高）。在Linux下，-20是最占用CPU的模式，20是空闲模式。</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.cpp" line="47"/>
        <source>Need restart</source>
        <translation>需要重启本软件</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.cpp" line="47"/>
        <source>New config will be loaded at next startup.</source>
        <translation>新的配置在下次重启时生效。</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.cpp" line="55"/>
        <location filename="gui/dlgsettings.cpp" line="75"/>
        <source>Only Windows</source>
        <translation>只有windows才需要配置</translation>
    </message>
    <message>
        <location filename="gui/dlgsettings.cpp" line="55"/>
        <location filename="gui/dlgsettings.cpp" line="75"/>
        <source>for Linux please install depends from package manage .</source>
        <translation>对Linux用户，请使用发行版对应的软件库(Qt, uhd, iio,fftw，pcap等）编译并运行。</translation>
    </message>
</context>
<context>
    <name>FormStatus</name>
    <message>
        <location filename="watchdog/status_charts/formstatus.ui" line="14"/>
        <location filename="watchdog/status_nocharts/formstatus.ui" line="14"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
</context>
<context>
    <name>HandbookView</name>
    <message>
        <location filename="gui/handbookview.ui" line="14"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
    <message>
        <location filename="gui/handbookview.ui" line="22"/>
        <source>Backward</source>
        <translation>回退</translation>
    </message>
    <message>
        <location filename="gui/handbookview.ui" line="29"/>
        <source>Forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <location filename="gui/handbookview.ui" line="36"/>
        <source>URL</source>
        <translation>网址</translation>
    </message>
    <message>
        <location filename="gui/handbookview.cpp" line="33"/>
        <source># Documents Does NOT Exist<byte value="xd"/>
<byte value="xd"/>
You can write documents with markdown in any place below:<byte value="xd"/>
<byte value="xd"/>
</source>
        <translation># 文档尚未完善

您可以在下面三种位置之一帮助taskBus完善文档:

</translation>
    </message>
</context>
<context>
    <name>PDesignerView</name>
    <message>
        <location filename="gui/pdesignerview.ui" line="17"/>
        <location filename="gui/pdesignerview.cpp" line="78"/>
        <source>Designer</source>
        <translation>设计师</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="79"/>
        <source>zoom_In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="82"/>
        <source>zoom_In, Ctrl+=</source>
        <translation>放大(^=)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="85"/>
        <source>Ctrl+=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="94"/>
        <source>zoom_Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="97"/>
        <source>zoom_Out, Ctrl+-</source>
        <translation>缩小,Ctrl+-</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="100"/>
        <source>Ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="109"/>
        <source>zoom_orgin</source>
        <translatorcomment>原始大小</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="118"/>
        <source>Delete selected node</source>
        <translatorcomment>删除选中</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="121"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="130"/>
        <source>debug on</source>
        <translatorcomment>调试开</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="139"/>
        <source>debug off</source>
        <translatorcomment>调试关</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="148"/>
        <source>Copy</source>
        <translatorcomment>复制</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="151"/>
        <source>Copy, Ctrl+C</source>
        <translation>复制(^C)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="154"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="163"/>
        <source>Paste</source>
        <translatorcomment>粘贴</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="166"/>
        <source>Paste,Ctrl+V</source>
        <translation>粘贴(^V)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="169"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="178"/>
        <source>Cut</source>
        <translatorcomment>剪贴</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="181"/>
        <source>Cut,Ctrl+X</source>
        <translation>剪切(^X)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="184"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="193"/>
        <source>DeleteLine</source>
        <translation>删除本管脚的连线</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="196"/>
        <source>DeleteLine,Ctrl+D</source>
        <translation>删除本管脚的连线,Ctrl+D</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="199"/>
        <source>Ctrl+D</source>
        <oldsource>Ctrl+L</oldsource>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="307"/>
        <source>&amp;rename</source>
        <translation>重命名(&amp;R)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="310"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="208"/>
        <source>PinUp</source>
        <translation>管脚上移</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="211"/>
        <source>pins Up(^Up)</source>
        <translation>管脚向上移动(^Up)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="214"/>
        <source>Ctrl+Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="223"/>
        <source>PinDown</source>
        <translation>管脚下移</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="226"/>
        <source>pins down(^Down)</source>
        <translation>管脚向下移动(^Down)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="229"/>
        <source>Ctrl+Down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="238"/>
        <source>PinSide</source>
        <translation>管脚交换方向</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="241"/>
        <source>Move Pins to another side(^Left, ^Right)</source>
        <translation>将管脚翻转到模块图标的另一边(^Left,^Right)</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="244"/>
        <source>Ctrl+Left, Ctrl+Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="253"/>
        <source>NiceUp</source>
        <translation>提高进程优先级</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="256"/>
        <source>Process priority Up</source>
        <translation>提高选中模块的进程优先级</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="265"/>
        <source>NiceDown</source>
        <translation>降低进程优先级</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="268"/>
        <source>Process priority Down</source>
        <translation>降低所给模块的进程优先级</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="277"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="280"/>
        <source>Undo changes</source>
        <translation>撤销修改</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="283"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="292"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="295"/>
        <source>Redo changes</source>
        <translation>重做修改</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.ui" line="298"/>
        <source>Ctrl+Shift+Z</source>
        <translation></translation>
    </message>
    <message>
        <source>Mod does not exist.</source>
        <translation type="vanished">模块尚未注册。</translation>
    </message>
    <message>
        <source>Mod %1 does not exists.</source>
        <translation type="vanished">模块 %1 尚未在平台注册。</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="59"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="161"/>
        <source>Save?</source>
        <translation>保存？</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="161"/>
        <source>Project has been modified, continue without saving ?</source>
        <translation>项目已经编辑，放弃保存继续操作？</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="301"/>
        <source>Close without Saving?</source>
        <translation>不保存就关闭吗？</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="301"/>
        <source>Project has been modified, Close it anyway?</source>
        <translation>项目已经编辑了，是否不保存就继续关闭？</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="916"/>
        <source>Set Friendly Name</source>
        <translation>设置友好名称</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="916"/>
        <source>Tooltip</source>
        <translation>名称</translation>
    </message>
    <message>
        <source>Project has been modified, Close it?</source>
        <translation type="vanished">项目已经被编辑，是否保存？</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="689"/>
        <location filename="gui/pdesignerview.cpp" line="699"/>
        <location filename="gui/pdesignerview.cpp" line="714"/>
        <location filename="gui/pdesignerview.cpp" line="724"/>
        <source>Already at min</source>
        <translation>已经抵达底部</translation>
    </message>
    <message>
        <location filename="gui/pdesignerview.cpp" line="689"/>
        <location filename="gui/pdesignerview.cpp" line="699"/>
        <location filename="gui/pdesignerview.cpp" line="714"/>
        <location filename="gui/pdesignerview.cpp" line="724"/>
        <source>Pin order value already at min, you can move other pins UP</source>
        <translation>管脚优先级已经最小。您可以增大其他管脚优先级来达到同样目的</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="core/taskcell.cpp" line="352"/>
        <source>Global</source>
        <oldsource>Golbal</oldsource>
        <translation>全局</translation>
    </message>
</context>
<context>
    <name>WatchMemModule</name>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="29"/>
        <source>PID</source>
        <translation>进程ID</translation>
    </message>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="29"/>
        <source>Name</source>
        <translation>进程名</translation>
    </message>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="29"/>
        <source>Memory</source>
        <translation>内存占用(MB)</translation>
    </message>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="30"/>
        <source>pack_reci</source>
        <translation>包接收</translation>
    </message>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="30"/>
        <source>pack_sent</source>
        <translation>包发送</translation>
    </message>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="30"/>
        <source>bytes_reci</source>
        <translation>字节接收</translation>
    </message>
    <message>
        <location filename="watchdog/watchmemmodule.cpp" line="30"/>
        <source>bytes_sent</source>
        <translation>字节发送</translation>
    </message>
</context>
<context>
    <name>taskBus</name>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="14"/>
        <source>taskBus</source>
        <translation>进程总线工作室</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="98"/>
        <source>Pro&amp;ject</source>
        <oldsource>&amp;Project</oldsource>
        <translation>工程(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Moudles</source>
        <translation type="vanished">模块(&amp;M)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="113"/>
        <source>&amp;View</source>
        <translation>视图(&amp;V)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="120"/>
        <location filename="gui/taskbusplatformfrm.ui" line="603"/>
        <source>&amp;Help</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="133"/>
        <source>General</source>
        <translatorcomment>通用</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="172"/>
        <source>Mod&amp;ules</source>
        <oldsource>Modules</oldsource>
        <translation>模块(&amp;u)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="201"/>
        <source>default</source>
        <translation>default</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="219"/>
        <source>Load taskbus Moudles from disk</source>
        <translation>从磁盘文件向当前库添加模块</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="233"/>
        <source>Delete Module</source>
        <translation>清空模块表</translation>
    </message>
    <message>
        <source>D</source>
        <translation type="vanished">D</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="258"/>
        <source>Save Plan</source>
        <translation>保存模块表</translation>
    </message>
    <message>
        <source>S</source>
        <translation type="vanished">S</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="244"/>
        <source>Load Plan</source>
        <translation>加载模块表</translation>
    </message>
    <message>
        <source>L</source>
        <translation type="vanished">L</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="322"/>
        <source>P&amp;roperties</source>
        <oldsource>Properties</oldsource>
        <translation>属性(&amp;R)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="385"/>
        <source>Messa&amp;ges</source>
        <oldsource>Messages</oldsource>
        <translation>消息(&amp;G)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="428"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="458"/>
        <source>comma&amp;nds</source>
        <translation>指令(&amp;n)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="493"/>
        <source>Show</source>
        <translation>显示</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="506"/>
        <source>&amp;Exit</source>
        <translation>退出(&amp;e)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="509"/>
        <source>Exit from this application.</source>
        <translation>退出当前程序。</translation>
    </message>
    <message>
        <source>&amp;Load Module</source>
        <translation type="vanished">载入模块(&amp;L)</translation>
    </message>
    <message>
        <source>Load a module executable file from disk.</source>
        <translation type="vanished">从磁盘的可执行文件模块载入元数据。</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="521"/>
        <source>Start &amp;project</source>
        <oldsource>&amp;Start project</oldsource>
        <translation>执行(&amp;P)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="524"/>
        <source>Start current project.</source>
        <translation>执行当前工程。</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="533"/>
        <source>&amp;New Project</source>
        <translation>新建工程(&amp;N)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="536"/>
        <source>Create New Project</source>
        <translation>建立新工程</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="586"/>
        <source>&amp;Hide</source>
        <translation>隐藏（&amp;H)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="598"/>
        <source>Save Project &amp;As...</source>
        <translation>工程另存为（&amp;A)...</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="612"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Save Project As...</source>
        <translation type="vanished">项目另存为...</translation>
    </message>
    <message>
        <source>&amp;Modules Window</source>
        <translation type="vanished">模块窗口(&amp;M)</translation>
    </message>
    <message>
        <source>Show/Hide Modules</source>
        <translation type="vanished">显示/隐藏模块窗口</translation>
    </message>
    <message>
        <source>&amp;Properties View</source>
        <oldsource>Properties View</oldsource>
        <translation type="vanished">属性窗口(&amp;P)</translation>
    </message>
    <message>
        <source>Show/Hide Prop window</source>
        <translation type="vanished">显示/隐藏属性窗口</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="545"/>
        <source>&amp;Open Project</source>
        <translation>打开工程(&amp;O)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="554"/>
        <source>&amp;Save Project</source>
        <translation>保存工程(&amp;S)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="559"/>
        <source>&amp;About</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="571"/>
        <source>S&amp;top project</source>
        <translation>终止运行(&amp;T)</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="574"/>
        <source>stop current project</source>
        <translation>停止执行当前工程</translation>
    </message>
    <message>
        <source>M&amp;essages</source>
        <translation type="vanished">消息(&amp;G)</translation>
    </message>
    <message>
        <source>&amp;hideWindow</source>
        <translation type="vanished">隐藏至任务栏图标（&amp;h）</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.ui" line="589"/>
        <source>hide window and show tray icon</source>
        <translation>把主界面隐藏起来，到状态栏图标里</translation>
    </message>
    <message>
        <source>D&amp;elete node</source>
        <translation type="vanished">删除模块(&amp;E)</translation>
    </message>
</context>
<context>
    <name>taskBusPlatformFrm</name>
    <message>
        <location filename="gui/taskbusplatformfrm.cpp" line="31"/>
        <location filename="gui/taskbusplatformfrm.cpp" line="32"/>
        <location filename="gui/taskbusplatformfrm.cpp" line="35"/>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="242"/>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="244"/>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="290"/>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="292"/>
        <source>All</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="225"/>
        <source>Init Modules...</source>
        <translation>初始化...</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="225"/>
        <source>Init modules from default_mods.text</source>
        <translation>正在从 default_mods.text 加载模块</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="237"/>
        <source>Remove All modules</source>
        <translation>清除所有模块</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="237"/>
        <source>This approach will clear current mod file.</source>
        <translation>请确认当前操作从模块库清除所有模块。</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="253"/>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="281"/>
        <source>save mod_def file as</source>
        <translation>模块表</translation>
    </message>
    <message>
        <source>taskBus</source>
        <translation type="vanished">进程总线工作室</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="227"/>
        <source>Succeed.</source>
        <translation>成功。</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="227"/>
        <source>Init modules from default_mods.text succeed!</source>
        <translation>成功从default_mods.text加载模块！</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.cpp" line="259"/>
        <source>Still running</source>
        <translation>项目仍在运行中</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.cpp" line="259"/>
        <source>Project is still running, please stop all projects first.</source>
        <translation>项目仍在运行，请先关闭后再退出。</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.cpp" line="265"/>
        <source>Close without saving?</source>
        <translation>不保存就关闭吗？</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.cpp" line="265"/>
        <source>Project has been modified, Close it anyway?</source>
        <translation>项目已经编辑了，是否不保存就继续关闭？</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm.cpp" line="354"/>
        <source>DOC:</source>
        <translation>文档:</translation>
    </message>
    <message>
        <source>Save?</source>
        <translation type="vanished">保存？</translation>
    </message>
    <message>
        <source>Project has been modified, Close it?</source>
        <translation type="vanished">项目已经被编辑，是否保存？</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="28"/>
        <source>Loading </source>
        <translation>加载 </translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="97"/>
        <source>Load %1 Modules , %2 succeed</source>
        <translation>加载 %1 个模块，%2 成功。</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="130"/>
        <location filename="gui/taskbusplatformfrm_modules.cpp" line="134"/>
        <source>load modules</source>
        <translation>载入模块</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_project.cpp" line="55"/>
        <location filename="gui/taskbusplatformfrm_project.cpp" line="108"/>
        <source>Save project</source>
        <translation>保存工程</translation>
    </message>
    <message>
        <location filename="gui/taskbusplatformfrm_project.cpp" line="85"/>
        <location filename="gui/taskbusplatformfrm_project.cpp" line="137"/>
        <source>Save Failed</source>
        <translation>保存失败</translation>
    </message>
</context>
<context>
    <name>taskBusPlatformFrm::taskBusPlatformFrm</name>
    <message>
        <location filename="gui/taskbusplatformfrm_project.cpp" line="183"/>
        <source>Open project</source>
        <translation>打开工程</translation>
    </message>
</context>
<context>
    <name>taskModule</name>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>Module Name</source>
        <translation>模块名</translation>
    </message>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>Class</source>
        <translation>类别</translation>
    </message>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>DefaultValue</source>
        <translation>默认值</translation>
    </message>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>Range</source>
        <translation>范围</translation>
    </message>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>Key</source>
        <translation>配置键</translation>
    </message>
    <message>
        <location filename="gui/taskmodule.cpp" line="24"/>
        <source>InstanceValue</source>
        <translation>实例值</translation>
    </message>
    <message>
        <source>Broken Json format:</source>
        <translation type="vanished">错误的JSON格式：</translation>
    </message>
</context>
<context>
    <name>taskNode</name>
    <message>
        <location filename="core/tasknode.cpp" line="78"/>
        <source>Error:File %1 does not exist. Please Load the module first.</source>
        <translation>错误：模块文件 %1 不存在。请在模块库先加载模块。</translation>
    </message>
</context>
<context>
    <name>taskProject</name>
    <message>
        <source>Mod %1 does not exists.</source>
        <translation type="vanished">模块 %1 尚未在平台注册。</translation>
    </message>
    <message>
        <location filename="core/taskproject.cpp" line="572"/>
        <source>source instance %1 is not a valid producer for subject %2.</source>
        <translatorcomment>来源实例%1不是专题%2的生产者。</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="core/taskproject.cpp" line="579"/>
        <source>input subject %1 has no valid recievers.</source>
        <translatorcomment>专题%1没有已登记的接收者。</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="core/taskproject.cpp" line="619"/>
        <source>output subject &quot;%1&quot; does not exits.</source>
        <translatorcomment>专题%1不存在。</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="core/taskproject.cpp" line="908"/>
        <source>No valid inside maps for inid %1 .</source>
        <translatorcomment>输入专题%1没有备选的内部映射。</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="core/taskproject.cpp" line="925"/>
        <source>outside input subject &quot;%1&quot; does not exits.</source>
        <translatorcomment>输出专题%1不存在。</translatorcomment>
        <translation></translation>
    </message>
</context>
</TS>
