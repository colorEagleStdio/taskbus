# Taskbus(兔巴士) - 一个跨平台的多进程合作框架

**Taskbus - A cross platform multi-process cooperation framework**

![TaskBusLogo](images/logo.png)

# 什么是Taskbus/What is TaskBus

Taskbus 是一种面向非专业开发者的跨平台多进程合作框架，具有进程切割、语言无关、编译器无关、架构无关四个特点。TaskBus 基于[增强管道数据流转技术(Enhanced Pipeline Data Routing, EPDR)](EPDR.md)，实现多个进程之间的标准输入输出数据交换流转。

Taskbus is a cross-platform multi-process cooperation framework for non-professional developers ,with four features of **process based, language independent, compiler independent, and architecture Independent**. Based on [Enhanced Pipeline Data Routing  (EPDR)](EPDR.md) technology , TaskBus enables Standard Input and Output data packages routing between multiple processes.

非专业开发者是一个泛泛的概念，可以理解为没有受过专业化的软件工程化训练的开发者。诸如需要频繁自行开发小工具进行算法验证的高校教研团队，以及深入某一领域（化工、机械、通信、电子等）进行数据分析，需要长期从事非消费类工具软件开发的工程师团队。

The **non-professional developer** is a general concept that can be understood as a developer without specialized software engineering training. For example, the university's research team, which needs to develop its own small tools for algorithm validation, and to conduct data analysis in a field (chemical, mechanical, communications, electronics, etc.), requires a long-term team of engineers working on non-consumer tool software development.


Taskbus 从感官上提供一种类似Simulink或GNU-Radio的模块化拖拽界面，可用于在通用计算机上实现准实时的处理逻辑。但是，从结构上，其与二者完全不同。Taskbus 对编译器、运行平台、开发语言不做要求。它通过定义一种功能发布与数据交换标准，提供一套进程管理平台，以便把不同语言开发的进程组合起来。

Taskbus provides a modular drag-and-drop interface like Simulink or gnu-radio that can be used to implement (quasi-)real-time processing logic on a general-purpose computer. However, structurally, it is completely different from the two. It provides a process management platform by defining a feature release and data exchange standard to combine processes developed in different languages.

[更多技术背景参看说明（简体中文）](taskbus_zh_CN.html)

[更多使用说明参看手册（简体中文）](handbook_zh_CN.html)

[For detailed information, please follow this link.(Eng)](taskbus_en.html)

![Project Demo](images/taskBus.png)


