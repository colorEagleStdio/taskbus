#Qt6 Widgets static link in MSYS2 need extra dependencies.
contains(QT, widgets){
MSYSTEM_PREFIX=$$(MSYSTEM_PREFIX)
greaterThan(MSYSTEM_PREFIX,' '){
contains(CONFIG, static) {
	message ("Perform Qt6 Static library with MSYS2 patch.")
	CONFIG += no_lflags_merge
	#LIBS += -ltiff  -lmng -ljpeg -ljbig -ldeflate  -lzstd -llerc -llzma  -lgraphite2 -lbz2 -lusp10 -lRpcrt4 -lsharpyuv -lOleAut32
}
}
}

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_LFLAGS_RELEASE -= -O1
QMAKE_LFLAGS_RELEASE += -O3
