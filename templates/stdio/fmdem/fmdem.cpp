#include <cstdio>
#include <cmath>
#ifdef WIN32
#include <io.h>
#include <fcntl.h>
#endif
#define Pi 3.14159265354

double angle(int x, int y)
{
	double angle = 0;
	if (x==0)
		angle = (y>0)? Pi/2:-Pi/2;
	else if (x >0 )
		angle = atan(y*1.0/x);
	else if (y >=0)
		angle = atan(y*1.0/x)+Pi;
	else
		angle = atan(y*1.0/x)-Pi;
	return angle;
}

int main()
{
#ifdef WIN32
	setmode(fileno(stdout), O_BINARY);
	setmode(fileno(stdin), O_BINARY);
#endif
	short buf[128][2];
	short out[128];
	int nPts = 0;
	int last_i = 0, last_q = 0;
	while ((nPts = fread(buf,4,128,stdin)))
	{
		for (int j=0;j<nPts;++j)
		{
			//d(phase)/d(t)  = freq
			int curr_i = buf[j][0];
			int curr_q = buf[j][1];
			int diff_i = last_i * curr_i + last_q * curr_q;
			int diff_q = last_q * curr_i - last_i * curr_q;			
			last_i = curr_i;
			last_q = curr_q;
			out[j] = angle(diff_i,diff_q)/Pi*4096;
		}
		fwrite(out,2,nPts,stdout);
		fflush(stdout);
	}
	return 0;
}
