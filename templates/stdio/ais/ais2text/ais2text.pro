TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ais/ais.cpp \
        ais/ais10.cpp \
        ais/ais12.cpp \
        ais/ais14.cpp \
        ais/ais15.cpp \
        ais/ais16.cpp \
        ais/ais17.cpp \
        ais/ais18.cpp \
        ais/ais19.cpp \
        ais/ais1_2_3.cpp \
        ais/ais20.cpp \
        ais/ais21.cpp \
        ais/ais22.cpp \
        ais/ais23.cpp \
        ais/ais24.cpp \
        ais/ais25.cpp \
        ais/ais26.cpp \
        ais/ais27.cpp \
        ais/ais4_11.cpp \
        ais/ais5.cpp \
        ais/ais6.cpp \
        ais/ais7_13.cpp \
        ais/ais8.cpp \
        ais/ais8_1_22.cpp \
        ais/ais8_1_26.cpp \
        ais/ais8_200.cpp \
        ais/ais8_366.cpp \
        ais/ais8_366_22.cpp \
        ais/ais8_367.cpp \
        ais/ais9.cpp \
        ais/ais_bitset.cpp \
        ais/decode_body.cpp \
        ais/vdm.cpp \
        main.cpp

HEADERS += \
	ais/ais.h \
	ais/decode_body.h \
	ais/vdm.h
