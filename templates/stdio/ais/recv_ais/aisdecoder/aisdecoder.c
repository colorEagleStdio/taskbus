/*
 *    main.cpp  --  AIS Decoder
 *
 *    Copyright (C) 2013
 *      Astra Paging Ltd / AISHub (info@aishub.net)
 *
 *    AISDecoder is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    AISDecoder uses parts of GNUAIS project (http://gnuais.sourceforge.net/)
 *
 */
/* This is a stripped down version for use with rtl_ais*/ 
#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <pthread.h>
//#include "config.h"
#include "sounddecoder.h"
#include "lib/callbacks.h"

#define MAX_BUFFER_LENGTH 2048
//#define MAX_BUFFER_LENGTH 8190

static char buffer[MAX_BUFFER_LENGTH];
static unsigned int buffer_count=0;
static int debug_nmea;
static int sock;
static int use_tcp = 0;
// messages can be retrived from a different thread
static pthread_mutex_t message_mutex;

// queue of decoded ais messages
struct ais_message {
    char *buffer;
    struct ais_message *next;
} *ais_messages_head, *ais_messages_tail, *last_message;

static void append_message(const char *buffer)
{
    struct ais_message *m = malloc(sizeof *m);

    m->buffer = strdup(buffer);
    m->next = NULL;
    pthread_mutex_lock(&message_mutex);

    // enqueue
    if(!ais_messages_head)
        ais_messages_head = m;
    else
        ais_messages_tail->next = m;
    ais_messages_tail = m;
    pthread_mutex_unlock(&message_mutex);
}

static void free_message(struct ais_message *m)
{
    if(m) {
        free(m->buffer);
        free(m);
    }
}

const char *aisdecoder_next_message()
{
    free_message(last_message);
    last_message = NULL;

    pthread_mutex_lock(&message_mutex);
    if(!ais_messages_head) {
        pthread_mutex_unlock(&message_mutex);
        return NULL;
    }

    // dequeue
    last_message = ais_messages_head;
    ais_messages_head = ais_messages_head->next;
    
    pthread_mutex_unlock(&message_mutex);
    return last_message->buffer;
}

int send_nmea( const char *sentence, unsigned int length);

void sound_level_changed(float level, int channel, unsigned char high) {
    if (high != 0)
        fprintf(stderr, "Level on ch %d too high: %.0f %%\n", channel, level);
    else
        fprintf(stderr, "Level on ch %d: %.0f %%\n", channel, level);
}

void nmea_sentence_received(const char *sentence,
                          unsigned int length,
                          unsigned char sentences,
                          unsigned char sentencenum) {
    append_message(sentence);

    if (sentences == 1) {
        if (send_nmea( sentence, length) == -1) abort();
        if (debug_nmea) fprintf(stderr, "%s", sentence);
    } else {
        if (buffer_count + length < MAX_BUFFER_LENGTH) {
            memcpy(&buffer[buffer_count], sentence, length);
            buffer_count += length;
        } else {
            buffer_count=0;
        }

        if (sentences == sentencenum && buffer_count > 0) {
            if (send_nmea( buffer, buffer_count) == -1) abort();
            if (debug_nmea) fprintf(stderr, "%s", buffer);
            buffer_count=0;
        };
    }
}

int send_nmea( const char *sentence, unsigned int length) {
	char bufout[1024];
	memcpy(bufout,sentence,length);
	bufout[length] = 0;
	puts(bufout);
	return 0;
}

int init_ais_decoder(char * host, char * port ,int show_levels,int _debug_nmea,int buf_len,int time_print_stats, int use_tcp_listener, int tcp_keep_ais_time, int add_sample_num){
	debug_nmea=_debug_nmea;
	use_tcp = use_tcp_listener;
	pthread_mutex_init(&message_mutex, NULL);
	if(debug_nmea)
		fprintf(stderr,"Log NMEA sentences to console ON\n");
	else
		fprintf(stderr,"Log NMEA sentences to console OFF\n");
    if (show_levels) on_sound_level_changed=sound_level_changed;
    on_nmea_sentence_received=nmea_sentence_received;
	initSoundDecoder(buf_len,time_print_stats,add_sample_num); 
	return 0;
}	

void run_rtlais_decoder(short * buff, int len)
{
	run_mem_decoder(buff,len,MAX_BUFFER_LENGTH);
}
int free_ais_decoder(void)
{
    pthread_mutex_destroy(&message_mutex);

    // free all stored messa ages
    free_message(last_message);
    last_message = NULL;
   
    while(ais_messages_head) {
        struct ais_message *m = ais_messages_head;
        ais_messages_head = ais_messages_head->next;

        free_message(m);
    }
    
    freeSoundDecoder();    
    return 0;
}

