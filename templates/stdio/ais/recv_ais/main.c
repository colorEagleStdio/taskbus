/*
 * Copyright (C) 2012 by Kyle Keen <keenerd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
typedef void* rtlsdr_dev_t;
#include "convenience.h"
#include "rtl_ais.h"
struct rtl_ais_config config;

void usage(void)
{
	fprintf(stderr,
			"recv_ais, a simple AIS tuner\n"
			"\t and generic dual-frequency FM demodulator\n\n"
			"(probably not a good idea to use with e4000 tuners)\n"
			"Use: recv_ais [options]\n"
			"\tsample_rate must be 1.6MHz frome stdin"
			"\t[-uint8 using uint8 instead of int16 input]"
			"\n");
	exit(1);
}

static volatile int do_exit = 0;
static void sighandler(int signum)
{
	signum = signum;
	fprintf(stderr, "Signal caught, exiting!\n");
	do_exit = 1;
}

int main(int argc, char **argv)
{
#ifndef WIN32
	struct sigaction sigact;

	sigact.sa_handler = sighandler;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	sigaction(SIGQUIT, &sigact, NULL);
	sigaction(SIGPIPE, &sigact, NULL);
#else
	signal(SIGINT, sighandler);
	signal(SIGTERM, sighandler);
#endif
	int opt;
	
	rtl_ais_default_config(&config);

	config.host = strdup("127.0.0.1");
	config.port = strdup("10110");

	while ((opt = getopt(argc, argv, "u?")) != -1)
	{
		switch (opt) {
		case 'u':
			config.use_uint8 = 1;
			break;		
		case '?':
		default:
			usage();
			return 2;
		}
	}

	config.filename = "-";

	if (config.edge) {
		fprintf(stderr, "Edge tuning enabled.\n");
	} else {
		fprintf(stderr, "Edge tuning disabled.\n");
	}
	if (config.use_uint8) {
		fprintf(stderr, "uint8 enabled.\n");
	} else {
		fprintf(stderr, "uint8 disabled.\n");
	}
	if (config.dc_filter) {
		fprintf(stderr, "DC filter enabled.\n");
	} else {
		fprintf(stderr, "DC filter disabled.\n");
	}
	if (config.rtl_agc) {
		fprintf(stderr, "RTL AGC enabled.\n");
	} else {
		fprintf(stderr, "RTL AGC disabled.\n");
	}
	if (config.use_internal_aisdecoder) {
		fprintf(stderr, "Internal AIS decoder enabled.\n");
	} else {
		fprintf(stderr, "Internal AIS decoder disabled.\n");
	}
	struct rtl_ais_context *ctx = rtl_ais_start(&config);
	if(!ctx) {
		fprintf(stderr, "\nrtl_ais_start failed, exiting...\n");
		exit(1);
	}
	/*
	  aidecoder.c appends the messages to a queue that can be used for a 
	  routine if rtl_ais is compiled as lib. Here we only loop and dequeue
	  the messages, and the puts() sentence that print the message is  
	  commented out. If the -n parameter is used the messages are printed from 
	  nmea_sentence_received() in aidecoder.c 
	  */
	while(!do_exit && rtl_ais_isactive(ctx)) {
#if _POSIX_C_SOURCE >= 199309L // nanosleep available()
		struct timespec five = { 0, 50 * 1000 * 1000};
#endif
		const char *str;
		if(config.use_internal_aisdecoder)
		{
			// dequeue
			while((str = rtl_ais_next_message(ctx)))
			{
				//puts(str); or code something that fits your needs
			}
		}
#if _POSIX_C_SOURCE >= 199309L // nanosleep available()
		nanosleep(&five, NULL);
#else
		usleep(50000);
#endif
	}
	rtl_ais_cleanup(ctx);
	return 0;
}
