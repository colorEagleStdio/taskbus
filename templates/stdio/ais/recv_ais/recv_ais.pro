TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        aisdecoder/aisdecoder.c \
        aisdecoder/lib/filter.c \
        aisdecoder/lib/hmalloc.c \
        aisdecoder/lib/protodec.c \
        aisdecoder/lib/receiver.c \
        aisdecoder/sounddecoder.c \
        convenience.c \
        main.c \
        rtl_ais.c

HEADERS += \
	aisdecoder/aisdecoder.h \
	aisdecoder/lib/callbacks.h \
	aisdecoder/lib/filter-i386.h \
	aisdecoder/lib/filter.h \
	aisdecoder/lib/hmalloc.h \
	aisdecoder/lib/protodec.h \
	aisdecoder/lib/receiver.h \
	aisdecoder/sounddecoder.h \
	convenience.h \
	rtl_ais.h
